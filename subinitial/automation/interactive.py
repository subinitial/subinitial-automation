import threading
import sys
import traceback
import socket
import json
import queue
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from testdefinition import TestDefinition

from . import VERSION_AUTOMATION


class Commander(threading.Thread):
    def __init__(self, test):
        self.test: "TestDefinition" = test
        self.running = True
        threading.Thread.__init__(self)
        self.daemon = True
        self._varnotify = (0, 0)

        self._askobj = None
        self._askqueue = queue.Queue()
        self.ctrlqueue = queue.Queue(maxsize=1)
        self._askid = 1

    def notify(self, obj):
        # called in test thread
        write, flush = self._varnotify
        if not write:
            return
        write(json.dumps(obj, default=lambda x: str(x)))
        write('\n')
        flush()

    def ask(self, obj, timeout=None):
        # called in test thread
        self._askobj = obj
        obj["_askid"] = self._askid
        # self.questions[self._askid] = obj
        self._askid += 1
        self.notify(obj)
        # self._question_queue.put(obj)
        return self._askqueue.get(timeout=timeout)

    def process_command(self, cmd_obj):
        # called in commander thread
        test = self.test

        # is this an ask response?
        if self._askobj:
            askid = cmd_obj.get("_askid")
            if askid is not None:
                if askid == self._askobj["_askid"]:
                    self._askobj = None
                    self._askqueue.put(cmd_obj)
                return None, False

        # then it must be an async cmd
        cmd = cmd_obj["cmd"]

        mid = cmd_obj.get("mid")
        resp = {"result": "ok", "cmd": cmd, "mid": mid}
        if cmd == "ctrl":
            op = cmd_obj.get("operation")
            if op:
                try: self.ctrlqueue.get_nowait()
                except Exception: pass
                try: self.ctrlqueue.put_nowait(op)
                except Exception: pass

        elif cmd == "guictrl1":
            try:
                gctrl = self.test.guictrl
                methname = cmd_obj["operation"]
                meth = getattr(gctrl, methname)
                resp["result"] = meth(*cmd_obj.get('args'), **cmd_obj.get('kwargs', {}))
                if cmd_obj.get("model", False):
                    resp["model"] = gctrl.model.serialize()
            
            except Exception as ex:
                resp["result"] = str(ex)
                return resp, False
            if not gctrl:
                return resp, False

        elif cmd == ("stepmod"):
            mod = cmd_obj.get("stepmod", None)
            tid = cmd_obj.get("tid", None)
            if mod is not None and tid is not None:
                if mod.startswith("break"):
                    if tid in self.test.breakpoints:
                        self.test.breakpoints.discard(tid)
                    elif type(tid) == int:
                        if tid < 0:
                            self.test.breakpoints = set()
                        else:
                            self.test.breakpoints.add(tid)
                    resp["response"] = {"breakpoints": list(self.test.breakpoints)}
                if mod.startswith("skip"):
                    step = self.test.find(tid)
                    skipped = self.test.skippoints
                    if tid in skipped:
                        skipped.remove(tid)
                    else:
                        # step.data.skip = True
                        skipped.add(tid)

                    resp["response"] = {"tid": tid, "skip": tid in skipped}
                elif mod.startswith("target"):
                    self.test._target_query = None if tid == self.test._target_query else tid
                    self.test._subset = None
                    resp["response"] = {"tid": tid, "target": self.test._target_query}
                elif mod.startswith("final"):
                    self.test._final_query = None if tid == self.test._final_query else tid
                    self.test._subset = None
                    resp["response"] = {"tid": tid, "final": self.test._final_query}

        elif cmd.startswith("s"):
            resp["response"] = "started"
        elif cmd.startswith("i"):
            resp["response"] = test._jstructure  # , indent=2, sort_keys=True)
        elif cmd.startswith("r"):
            resp["response"] = test._get_structure()
        elif cmd.startswith("a"):
            resp["response"] = self._askobj  # , indent=2, sort_keys=True)
        elif cmd.startswith("exe"):
            action_idx = cmd_obj.get("action_idx")
            resp["response"] = test.execute(action_idx)
        elif cmd.startswith("t"):
            resp["response"] = {"title": test.title, "lib_version": VERSION_AUTOMATION}
        elif cmd.startswith("q"):
            resp["response"] = "exiting"
            return resp, True

        return resp, False

    def stop(self):
        self.running = False


class RemoteCommander(Commander):
    def __init__(self, runner, address, port):
        Commander.__init__(self, runner)
        self.address = address
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def run(self):
        self.socket.bind((self.address, self.port))
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # self.socket.settimeout(1)
        self.socket.listen(0)
        sys.stderr.write("RemoteCommander: listening on {}:{}\n".format(self.address, self.port))
        while self.running:
            conn = None
            listening = True
            try:
                conn, address = self.socket.accept()
                listening = False
                sys.stderr.write("RemoteCommander: client connected\n")

                fin = conn.makefile(mode='r', encoding='utf-8', newline='\n')
                fout = conn.makefile(mode='w', encoding='utf-8', newline='\n')

                self._varnotify = fout.write, fout.flush

                while self.running:
                    cmd_obj = json.loads(fin.readline())
                    print("cmd_obj", cmd_obj)
                    resp_obj, exiting = self.process_command(cmd_obj)
                    if resp_obj:
                        self.notify(resp_obj)
                    if exiting:
                        break
            except Exception as ex:
                if not listening:
                    sys.stderr.write("RemoteCommander: " + str(ex) + '\n')
            finally:
                if conn:
                    conn.close()
                self.varnotify = (0, 0)
                if not listening:
                    sys.stderr.write("RemoteCommander: client disconnected\n")

    def stop(self):
        Commander.stop(self)
        self.socket.close()


class PipeCommander(Commander):
    def __init__(self, runner):
        Commander.__init__(self, runner)

    def run(self):
        fin, fout = None, None
        try:
            fin = open(3, mode='r', encoding='utf-8', newline='\n')
            fout = open(4, mode='w', encoding='utf-8', newline='\n')

            self._varnotify = fout.write, fout.flush

            while self.running:
                cmd_obj = json.loads(fin.readline())
                resp_obj, exiting = self.process_command(cmd_obj)
                if resp_obj:
                    self.notify(resp_obj)
                if exiting:
                    break
        except Exception as ex:
            sys.stderr.write("PipeCommander: " + str(ex) + '\n')  # + str(traceback.format_exc()))
        finally:
            if fout:
                fout.close()
            # fout = None
            if fin:
                fin.close()
            # fin = None

    def stop(self):
        Commander.stop(self)
        # with open(3, mode='w', encoding='utf-8', newline='\n') as f:
        #     f.write(json.dumps({"cmd": "q"}) + "\n")


class StdioCommander(Commander):
    HIDE_OUTPUT = False

    def __init__(self, runner):
        Commander.__init__(self, runner)

    def ask(self, obj, timeout=None):
        try:
            self._askobj = obj
            obj["_askid"] = self._askid
            self._askid += 1
            sys.stderr.write(json.dumps(obj))
            sys.stderr.write("\nresponse>")
            a = self._askqueue.get()
            return a
        except Exception as ex:
            print(ex)
            print("invalid json: {}", a)
            pass

    def run(self):
        if not self.HIDE_OUTPUT:
            self._varnotify = (sys.stderr.write, sys.stderr.flush)

        while self.running:
            intext = sys.stdin.readline().strip()
            if intext == "":
                intext = "s"
            cmd_obj = {"cmd": intext}
            if intext.startswith("{"):
                cmd_obj = json.loads(intext)
            resp_obj, exiting = self.process_command(cmd_obj)
            if resp_obj:
                self.notify(resp_obj)
            if exiting:
                break

    def stop(self):
        Commander.stop(self)
        # with open(1, mode='w', encoding='utf-8', newline='\n') as f:
        #     f.write("q\n")
