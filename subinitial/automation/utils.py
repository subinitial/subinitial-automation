# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework utility classes and functions
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.


import sys
import os
import re
import logging
import time
import csv
import ast
import traceback
from pathlib import Path
from typing import Callable, Any
from threading import Thread
from collections import OrderedDict

logger = logging.getLogger(__name__)


def get_datetime_pathsafe() -> str:
    return time.strftime("%Y-%m-%d_%H;%M;%S")


def progressbar(ratio: float, bar='=', head='>', resolution=25) -> str:
    prog = min(resolution, max(0, round(ratio * resolution)))
    remain = resolution - prog
    return "{:3}% [{}{}{}]".format(round(ratio * 100), prog * bar, head, remain * " ")

def get_arg(idx, default_val=None) -> str | None:
    return sys.argv[idx] if len(sys.argv) > idx else default_val


def in_tolerance(val, ideal, deviation):
    return (ideal - deviation) <= val <= (ideal + deviation)


class open_stubbornly:
    def __init__(self, name, mode='r', buffering=-1, encoding=None, errors=None, newline=None, closefd=True,
                 opener=None, prompt=None):
        if prompt is None:
            prompt = self.default_prompt

        while True:
            try:
                self.f = open(name, mode=mode, buffering=buffering, encoding=encoding, errors=errors, newline=newline,
                              closefd=closefd, opener=opener)
                return
            except FileNotFoundError:
                os.makedirs(os.path.split(name)[0])
            except Exception as ex:
                if not prompt(name):
                    raise ex

    def default_prompt(self, path):
        print("Opening ", path, "failed, try closing any programs that may be accessing it.")
        ui = input("Press Enter to try again, q to quit...")
        if ui is not None and ui.lower().startswith("q"):
            return False
        return True

    def __enter__(self):
        return self.f

    def __exit__(self, type, value, traceback):
        self.f.close()



def setup_logging(filename='log', level=logging.DEBUG):
    log_format = logging.Formatter('%(asctime)s - %(name)s:%(levelname)s: %(message)s')
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(log_format)
    logging.root.addHandler(ch)
    if filename is not None:
        fh = logging.FileHandler(filename + ".log")
        fh.setFormatter(log_format)
        logging.root.addHandler(fh)
    logging.root.setLevel(level)


def instantiate_template(*files: str | Path):
    for f in files:
        p = Path(f)
        if p.exists():
            return
        template = p.with_stem(f"{p.stem}.template")
        if template.exists():
            with open(template) as f:
                template_txt = f.read()
            with open(p, mode="w") as f:
                f.write(template_txt)
                f.flush()


class CsvBuilder:
    class Column:
        def __init__(self, column):
            self.str = ""
            self.idx = 0
            if type(column) is str:
                self.str = column.upper()[0]
            elif type(column) is int and (1 <= column <= 26):
                self.str = chr(ord('A') + column - 1)
            else:
                raise Exception('column must be a letter A-Z or a number 1-26')

            self.idx = ord(self.str) + 1 - ord('A')
            if not (0 <= self.idx <= 25):
                raise Exception('column: {} must be a letter A-Z or a number 1-26'.format(self.idx))

    def __init__(self, path: str, headers: list[list[Any]] | None=None, append=False):
        self.lineNumber = 0
        self.path: str = path
        self.headers: list[list[Any]] = headers
        self.append = append
        if headers is not None:
            if self.append and os.path.exists(path):
                return
            self.write_headers(headers, path)

    def _open_and(self, path: str, do_func, mode="w"):
        path = path or self.path
        with open_stubbornly(path, mode=mode, newline='', encoding="utf-8") as afile:
            if 'w' in mode:
                afile.write(bytes([0xEF, 0xBB, 0xBF]).decode())
            do_func(afile)

    def get_linecount(self, path=None) -> int:
        def countlines(afile):
            global linecount
            i = 0
            for i, l in enumerate(afile):
                pass
            return i + 1
        return self._open_and(path, countlines, mode='r')

    def write_headers(self, headers: list[list[Any]], path=None):
        def _write_table(csvfile):
            writer = csv.writer(csvfile, dialect='excel', delimiter=',')
            for row in headers:
                writer.writerow(row)
                self.lineNumber += 1
        self._open_and(path, _write_table, mode='w')

    def write_row(self, row: list[Any], append=None, path=None):
        if append is None:
            append = self.append
        mode = "a+" if append else "w"
        def _write_row(csvfile):
            writer = csv.writer(csvfile, dialect='excel', delimiter=',')
            writer.writerow(row)
            self.lineNumber += 1
        self._open_and(path, _write_row, mode=mode)

    def write_table(self, table: list[list[Any]], append=False, path=None):
        if append is None:
            append = self.append
        mode = "a+" if append else "w"
        def _write_table(csvfile):
            writer = csv.writer(csvfile, dialect='excel', delimiter=',')
            for row in table:
                writer.writerow(row)
                self.lineNumber += 1
        self._open_and(path, _write_table, mode=mode)

    @staticmethod
    def make_average_row(rowstart: int, rowend: int, colstart: str | int, colend: str | int):
        colstart = CsvBuilder.Column(colstart)
        colend = CsvBuilder.Column(colend)

        row = list()
        for i in range(colstart.idx, colend.idx + 1):
            coltext = chr(i - 1 + ord('A'))
            cell = '=AVERAGE({}{}:{}{})'.format(coltext, str(rowstart), coltext, str(rowend))
            row += [cell]
        return row


class NodeString:
    treefill = "│ "
    childglyph = "├─"
    lastchildglyph = "└─"

    @staticmethod
    def node(depth: int, text: str):
        return NodeString.treefill * depth + text

    @staticmethod
    def notes_pre(depth: int):
        return NodeString.treefill * (depth + 1)

    @staticmethod
    def notes(depth: int, text: str):
        return NodeString.treefill * (depth + 1) + text

    @staticmethod
    def child(depth: int, text: str, islast=False):
        if islast:
            return NodeString.lastchild(depth, text)
        else:
            return NodeString.treefill * depth + NodeString.childglyph + text

    @staticmethod
    def lastchild(depth: int, text: str):
        return NodeString.treefill * depth + NodeString.lastchildglyph + text


def run_concurrently(*list_of_functions) -> list[Any]:
        list_of_threads = []
        list_of_retvals = [None] * len(list_of_functions)

        i = 0
        for aFunc in list_of_functions:
            if type(aFunc) == tuple or type(aFunc) == list:
                def _wrapfunc(idx=i):
                    list_of_retvals[idx] = aFunc[0](*aFunc[1:])
                wrapfunc = _wrapfunc
            else:
                def _wrapfunc(idx=i):
                    list_of_retvals[idx] = aFunc()
                wrapfunc = _wrapfunc
            new_thread = Thread(target=wrapfunc)
            new_thread.start()
            list_of_threads += [new_thread]
            i += 1

        for aThread in list_of_threads:
            aThread.join()

        return list_of_retvals


class LoopGoverner:
    def __init__(self, min_looptime_s: float):
        self.min_looptime_s = min_looptime_s
        self.next_time = 0

    def wait(self):
        delay_time = self.next_time - time.time()
        if delay_time > 0:
            time.sleep(delay_time)
        self.next_time = time.time() + self.min_looptime_s


class Timer:
    def __init__(self):
        self.start_time = time.time()

    def restart(self):
        self.start_time = time.time()

    def get_s_ellapsed(self, rounding: int | None=None) -> float:
        val = time.time() - self.start_time
        if rounding is not None:
            return round(val, rounding)
        return val

    def get_ms_ellapsed(self) -> int:
        return int(1000 * (time.time() - self.start_time))


def text_eval(text, rtype=None) -> Any:
    try:
        # Leave non-string types be
        if type(text) != str:
            return text
        # Empty strings (whitespace only) count as None
        text = text.strip()
        if not len(text):
            if rtype:
                return rtype()
            return None

        # fix quotation marks
        text = text.replace('“', '"').replace('”', '"').replace("‘", "'").replace("’", "'")

        # quoted strings
        if text[0] == '"':
            return text.strip('"')
        if text[0] == "'":
            return text.strip("'")

        if rtype == str:
            return text

        # enumerables
        if text[0] in ["[", "(", "`", "{"]:
            return ast.literal_eval(text.strip("`"))
        if "," in text:
            return ast.literal_eval(text)
        if text.startswith("b'") and text.endswith("'"):
            return ast.literal_eval(text)

        # numbers
        if text[0] in '-1234567890.':
            if "." in text or "e" in text or "E" in text:
                num = float(text)
            else:
                num = int(text)
            if rtype:
                return rtype(num)
            return num

        # booleans
        lowers = text.lower()
        if lowers == "true":
            return True
        if lowers == "false":
            return False
    except Exception as ex:
        pass
    return text


class ArgsDict(OrderedDict):
    def serialize(self):
        if getattr(self, "_ser", None) is None:
            setattr(self, "_ser", tuple(self.items()))
        return self._ser


def _parse_args(args) -> tuple[ArgsDict, ArgsDict]:
    """parses cmd line arguments searching for -C and -F flags

    :return: dictionary of field entries, dictionary of config values
    :rtype: dict(), dict()
    """
    c = ArgsDict()
    f = ArgsDict()
    i = 0
    while i < len(args):
        arg = args[i]
        i += 1

        if arg.startswith("-C"):
            d = c
        elif arg.startswith("-F"):
            d = f
        else:
            continue

        if len(arg) == 2:
            # the value for the -C or -F lies in the next subsequent arg scalar
            if i >= len(args):
                break
            arg = args[i]
            i += 1
        else:
            arg = arg[2:]

        keyval = arg.split("=", maxsplit=1)
        if len(keyval) == 2:
            key = keyval[0].strip()
            d[key] = keyval[1].strip('"')

    for key, val in c.items():
        c[key] = text_eval(val)
    return f, c

# Cache the -F args and the -C args once for others to use
argsF, argsC = _parse_args(sys.argv)


class Task:
    def __init__(self, func: Callable, *args, **kwargs):
        """Start a task to run a function in another thread.

        Note: task threads will continue running until the func function returns or the Python process exits.

        :param func: function to call in the task's Thread, the function signature should be: fn(arg)
        :type func: function
        :param args: arbitrary arguments to pass to the function
        :param kwargs: arbitrary keyword arguments to pass to the function
        """
        self.retval: Any = None
        self.thread = Thread(target=self._task_context, args=(func, args, kwargs))
        self.thread.daemon = True
        self.thread.start()

    def _task_context(self, func, args, kwargs):
        
        try:
            self.retval = func(*args, **kwargs)
        except Exception as ex:
            self.retval = ex, traceback.format_exc()
            print(self.retval)


    def is_finished(self) -> bool:
        """check if task is still running or has finished

        :return: return True if task is finished, False if it is still running
        """
        return not self.thread.is_alive()

    def wait(self, timeout: float=30) -> Any:
        """Wait for task to complete and provide the task's return value.

        Note: If an exception occured during the task's operation then the return value will be
            subsituted for a tuple as follows: (Exception, traceback_string)

        :param timeout: time after which to give up waiting for the task to complete, pass None if no timeout should occur.
            If a timeout occurs, task thread will continue to run until the Python process exits.
        :type timeout:float
        :return: return value of the task
        """
        self.thread.join(timeout)
        if self.thread.is_alive():
            raise TimeoutError("Waiting for tasks timed out after {}s. Warning: tasks may still be running.".format(timeout))
        return self.retval


class TaskList:
    def __init__(self):
        self.tasks: list[Task] = []
        self._task_retvals = []

    def start(self, func: Callable, *args: Any, **kwargs) -> Task:
        """Start a task to run a function in another thread.

        Note: task threads will continue running until the func function returns or the Python process exits.

        :param func: function to call in the task's Thread, the function signature should be: fn(arg)
        :type func: function
        :param args: arbitrary arguments to pass to the function
        :param kwargs: arbitrary keyword arguments to pass to the function
        :returns: Task object representing the running task
        """
        task = Task(func, *args, **kwargs)
        self.tasks.append(task)
        return task

    def is_finished(self) -> bool:
        """check if all tasks have finished

        :return: return True if all tasks have finished, False if at least one task is still running
        """
        for task in self.tasks:
            if not task.is_finished():
                return False
        return True

    def wait(self, timeout: float=30) -> list[Any]:
        """Wait for all started tasks to complete and return a list of each task's return value.

        Note: If an exception occured during the task's operation then the return value will be
            subsituted for a tuple as follows: (Exception, traceback_string)

        :param timeout: time after which to give up waiting for the tasks to complete, pass None if no timeout should occur.
            If a timeout occurs, task threads will continue to run until the Python process exits.
        :type timeout:float
        :return: list of return values (or Exceptions,Traceback values) of each task in the order in which they were started
        :rtype: list[]
        """
        t_start = time.time()
        retvals = []
        for t in self.tasks:
            tleft = timeout
            if timeout is not None:
                tleft = timeout - (time.time() - t_start)
                if tleft <= 0:
                    raise Exception(
                        "Waiting for tasks timed out after {}s. Warning: tasks may still be running.".format(timeout))
            retvals.append(t.wait(tleft))
        self.tasks.clear()
        return retvals


class ConnectionLogic:
    def __init__(self, is_asynchronous=False, timeout=30, depends_on=tuple(), tag=None, retries=0):
        self.connected = False
        self.error = None
        self.is_asynchronous = is_asynchronous
        self.timeout = timeout
        self.tag = tag
        self.depends_on = depends_on
        self.retries = retries

    def connect(self) -> None | str:
        """Implement conneciton logic in this method

        :param arg: arbitrary argument to be used in making the connection
        :return: None if no errors, otherwise return error message or value
        """
        pass

    def disconnect(self) -> None | str:
        """Implement disconneciton logic in this method

        :param arg: arbitrary argument to be used in breaking the connection
        :return: None if no errors, otherwise return error message or value
        """
        pass

    def run_connect(self, arg=None) -> tuple[Exception, str] | None:
        """Connects if a connection isn't already present,

        :return: None if no error, otherwise return error message or value"""
        if self.error is None and not self.connected:
            tries = self.retries + 1
            while tries:
                try:
                    self.error = self.connect()
                except Exception as ex:
                    self.error = ex, traceback.format_exc()
                if self.error is None:
                    self.connected = True
                    break
                else:
                    tries -= 1
                    if tries:
                        try:
                            self.disconnect()
                        except:
                            pass
        return self.error

    def reset(self):
        self.error = None
        self.connected = False

    def _dependency_check(self, conmgr):
        for tag in self.depends_on:
            dependency = conmgr.by_tag.get(tag)
            if dependency and not dependency.connected:
                return False
        return True

    def run_disconnect(self, arg=None) -> tuple[Exception, str] | None:
        """Disconnects if a connection is already present,

        :return: None if no error, otherwise return error message or value"""
        if self.error is None and self.connected:
            try:
                self.error = self.disconnect()
            except Exception as ex:
                self.error = ex, traceback.format_exc()
            if self.error is None:
                self.connected = False
        return self.error


class PingConnectionLogic(ConnectionLogic):
    def get_host_and_name(self) -> tuple[str, str]:
        return "", "N/A"
    
    def make_connection(self):
        pass

    def connect(self) -> str | None:
        # Return None if connection was successful, or a string describing the error if somethign went wrong.
        
        ipv4_addr, device_name = self.get_host_and_name()

        logger.debug(f"Identifying {device_name}")
        try:
            # Verify that the device is present before attempting to connect
            if not ping(ipv4_addr):
                if not ping(ipv4_addr): 
                    return f'Failed to verify presence of {device_name} with IPv4 address - {ipv4_addr}, verify that the device is connected.'
        
            logging.debug(f"{device_name} connecting...")
            self.make_connection()
            logging.debug(f"{device_name} connected")
        except Exception as ex:
            return f'Failed connecting to {device_name}. Exception: "{ex}"'

        return None # Success case


class ConnectionManager:
    def __init__(self):
        self.connections: list[ConnectionLogic] = []
        self.by_tag: dict[str, ConnectionLogic] = {}
        self.errors_were_detected = False
        self.errors = []

    @staticmethod
    def stringify_errors(operation, errors):
        rets = []
        for r in errors:
            if r is None:
                continue
            elif type(r) == tuple and len(r) == 2 and isinstance(r[0], BaseException) and type(r[1]) == str:
                rets.append(str(r[0]) + "\n\t\t" + r[1].rstrip().replace("\n", "\n\t\t"))
            else:
                rets.append(str(r))

        if rets:
            rets.insert(0, "{} failed:".format(operation))
            return "\n\t-".join(rets)
        return ""

    def errors_tostr(self):
        is_connected = False
        if len(self.connections):
            is_connected = self.connections[0].connected

        if self.errors_were_detected:
            operation = "Disconnect" if is_connected else "Connect"
        else:
            operation = "Connect" if is_connected else "Disconnect"
        return ConnectionManager.stringify_errors(operation, self.errors)

    def connect(self):
        """Connects all ConnectionLogic instances that were added to this ConnectionManager with .add()"""
        self.errors = []
        self.errors_were_detected = False

        tasks = TaskList()
        timeout = 30
        for c in self.connections:
            if c.is_asynchronous:
                if not c._dependency_check(self):
                    continue
                timeout = max(c.timeout, timeout)
                tasks.start(c.run_connect)
            else:
                self.errors += tasks.wait(timeout=timeout)
                if not c._dependency_check(self):
                    continue
                timeout = max(c.timeout, 30)
                self.errors.append(c.run_connect())
        self.errors += tasks.wait(timeout=timeout)

        for c in self.connections:
            if c.error is not None or not c.connected:
                self.errors_were_detected = True
                break

        if self.errors_were_detected:
            for c in self.connections:
                c.run_disconnect()
            if all(not x for x in self.errors):
                self.errors = ["Connection sequencing invalid, check depends_on / is_asynchronous"]
            for c in self.connections:
                c.reset()
        else:
            self.errors = None
        return self.errors

    def add(self, connection):
        self.connections.append(connection)
        if connection.tag is not None:
            self.by_tag[connection.tag] = connection

    def disconnect(self):
        """Disconnects all ConnectionLogic instances that were added to this ConnectionManager with .add()"""
        self.errors = []
        for c in reversed(self.connections):
            self.errors.append(c.run_disconnect())
            if c.error is not None:
                self.errors_were_detected = True
        if self.errors_were_detected:
            return self.errors
        return None


def ping(host: str, timeout=1) -> bool:
    """Returns True if host responds to a ping request"""
    import subprocess
    import platform
    # Ping parameters as function of OS
    if platform.system().lower() == "windows":
        output = subprocess.getoutput(["ping", "-n", "1", "-w", str(timeout * 1000), host])
        return "ttl" in output.lower()
    else:
        output = subprocess.getoutput("ping -c1 -w{} {}".format(int(timeout), host, shell=True))
        return len(re.findall(r"\n\d+\s+bytes", output)) > 0


class TriageRefs:
    weights = {
        "J": -20,
        "U": -10,
        "C": 10,
        "R": 20
    }

    def __init__(self, text):
        if type(text) != str:
            items = []

            def drill(textitems):
                for i in textitems:
                    if type(i) == str:
                        items.append(i)
                    else:
                        drill(i)
            drill(text)
            text = " | ".join(items)
        self.text = text
        self.priority = []
        self.unusual = []
        self.refs = []
        """:type: list[tuple(int, str, int, str)]"""
        # (weight, prefix, index, reference designator)
        self.crunch()

    def crunch(self):
        refmatch = re.compile(r"([A-Z]+)(\d+)")
        for ref in self.text.split("|"):
            ref = ref.strip()
            if not ref:
                continue
            if ref[0] == "@":
                self.priority.append(ref[1:])
                continue
            m = refmatch.fullmatch(ref)
            if not m:
                self.unusual.append(ref)
                continue

            prefix, index = m.group(1), m.group(2)
            self.refs.append((
                self.weights.get(prefix, 0),
                prefix,
                int(index),
                ref
            ))
        self.refs.sort()

    def tolist(self) -> list[str]:
        return self.priority + self.unusual + list(x[3] for x in self.refs)

    def tostr(self) -> str:
        return " | ".join(self.tolist())
