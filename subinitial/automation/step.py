# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework Step class and associated Field and Report classes
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import time
import sys
import traceback
import re
from datetime import datetime
import threading
import logging
from enum import IntEnum
from typing import Callable, Any, Iterator, TYPE_CHECKING, Union, Type

from .exceptions import *
from .assertions import *
from .utils import Task
if TYPE_CHECKING:
    from .testdefinition import TestDefinition
    from .reporters import Reporter
    from .testconfig import StepTable


logger = logging.getLogger(__name__)


_step_dir = ("step_tags", "step_id", "step_is_persistent", "step_is_resilient",
             "step_is_independent", "title", "skip", "step_resultstyle",
             "step_retry", "step_retry_children", "step_teardown_before_retry",
             "step_is_not_applicable", "step_triage_refs", "step_triage_msg",
             "step_purpose", "step_is_documented", "step_duration_estimate",
             "step_force_sync", "step_async_children", "step_is_requirement",
             "step_repeat")  #, "Skip", "SKIP")


def _freeze_setaddr(self, key: str, val: Any):
    """ Try to set a Step.Data instance's key if it's within the non-frozen fields. """
    fd = getattr(self, "__freezedir", None)
    if fd and key not in fd:
        raise AttributeError("Test step's class '{}': '{}' is not defined".format(type(self).__name__, key))
    object.__setattr__(self, key, val)


def _freeze_instance_data(inst, t: type):
    """ Freeze a Step.Data instance so that no new keys can be added or monkey patched onto it. """
    _dir = set(dir(inst))
    _dir.update(_step_dir)
    inst.__freezedir = _dir
    t.__setattr__ = _freeze_setaddr


class DefaultThrow:
    """Argument to indicate test tree queries should throw an exception if they fail"""
    pass


class StepCollection(list):
    def __init__(self, step: "Step"):
        self.cv: threading.Condition = None
        self.active_child_idx: int = None
        self.step: Step = step
        list.__init__(self)

    def async_groups(self) -> Iterator[list["Step"]]:
        """
        Return an iteration of step-lists [Step, Step, ...] that can run asynchronously
        """
        async_steps = []
        for step in self:
            if step.step_force_sync:
                if async_steps:
                    yield async_steps
                    async_steps.clear()
                yield [step]
            else:
                async_steps.append(step)
        yield async_steps

    @property
    def parent(self) -> Union["TestDefinition", "Step"]:
        """ Get a reference to this step's parent in the test tree """
        return self.step.parent

    @parent.setter
    def parent(self, value: Union["TestDefinition", "Step"]):
        self.step.parent = value

    def add(self, *steps: "Step"):
        for child in steps:
            if not isinstance(child, Step):
                raise ValueError("child must be a type derived from Step")
            self.append(child)
            child.parent = self.step

    @staticmethod
    def _copy(src_step: "Step", dst_step: "Step"):
        for step in src_step.steps:
            step2 = step._copy()
            dst_step.add_steps(step2)
            StepCollection._copy(step, step2)

    def __iter__(self) -> Iterator["Step"]:
        return list.__iter__(self)


class Step:
    """
    The test developer subclasses Step to define a unique logical step of a test procedure.
    A subclassed step should define a particular set of data, pass/fail criteria, procedure
    logic, and teardown logic.
    These four main components are briefly summarized below:
        - class Data:
            A step's _data_ object can represent both configuration data and limits as inputs,
            and measured or results data as outputs of a test step. Within a subclassed step
            the test designer can define an appropriate set of fields that correspond to
            input/configuration and output/measurements specific to that step's needs. The
            framework will construct a single instance of the Data class, and gather default starting
            values of the fields via a cascading heirarchy. This heirarchy from low to high:
                -Data constructor
                -Step constructor keyword arguments
                -config.csv StepTables
            The data object is passed as a parameter to the various Step methods for convenience, however
            it is always accessible as an instance field of the step (e.g. self.data)

        - def criteria(self, data: Data):
            The criteria method procedurally defines all the pass/fail criteria of the test step.
                -Ususally criteria() can consist entirely of one or many self.assert_* method calls
                    to evaluate data retrieved from the procedure() method
                -A reference to the step's data object is passed as a parameter for convenience
                -Use fields on the data object to organize assertion limits and procedure results
                -Note: avoid conditionals and loops within this method until you understand
                    when and how the Automation Framework invokes it. Refer to the criteria method
                    documentation for more details

        - def procedure(self, data: Data):
            The procedure method defines the test stimulation and measurement aquisition specific
            for this test step. It's objective is to take any configuration inputs provided in the
            step's _data_ object and store results back into the step's _data_ object for _criteria()_
            to evaluate. The _data_ object's specific input and output fields are defined by you,
            the test designer.

        - def teardown(self, data: Data):
            The teardown method should define a context independent way to turn off conditions setup by the 
            procedure. The procedure may include invalid Python code, bugs, or conditions in which Python 
            exceptions can halt its operation. Any unforseen corner-cases might leave a physical test setup 
            in an unknown and potentially dangerous state. The teardown method is called after the procedure, 
            regardless of wether the procedure completed or crashed half-way through. Do your best writing 
            resilient teardown logic, but even if the teardown code crashes, the framework will carry on 
            doing its best to call parent step's _teardown()_s, and finally the toplevel test procedure's 
            _disconnect()_ method.

    """

    class Stage(IntEnum):
        PRE = 0
        PROCEDURE = 1
        CRITERIA = 2
        POST = 3
        ERROR = 4

    @staticmethod
    def get_datetime(fmt="%y-%m-%d_%H-%M-%S"):
        return datetime.now().strftime(fmt)

    class Data:
        """Default empty Data class used when Step class doesn't declare its own"""
        pass

    @property
    def skip(self):
        """
        If True, this step's procedure is skipped
        """
        return getattr(self.data, "skip", False)

    @skip.setter
    def skip(self, val: bool):
        self.data.skip = val

    @property
    def step_is_requirement(self) -> bool:
        """
        If True then self.teardown() is called after all siblings complete.
        Requirement steps teardowns are called in revese order of their appearance.
        If a target step is designated, all requirement steps leading up to the target 
        are run and not skipped.
        """
        return getattr(self.data, "step_is_requirement", False)

    @property
    def step_is_resilient(self) -> bool:
        """ 
        If True then all child tests are run even if one fails. If false, stop on first failure 
        """
        return getattr(self.data, "step_is_resilient", False)

    @property
    def step_is_independent(self) -> bool:
        """ 
        If True then child test failures don't force this test Step's result to fail.
        This test Step's result is only dependent on it's own self.criteria() method result.
        """
        return getattr(self.data, "step_is_independent", False)

    @property
    def children_are_independent(self) -> bool:
        """ 
        If True then all child tests are run even if this Step fails. If false, children do not run if this Step fails.
        """
        return getattr(self.data, "children_are_independent", False)
    
    @property
    def step_is_persistent(self) -> bool:
        """ 
        If True then self.teardown() is called after Test procedure completes 
        """
        return getattr(self.data, "step_is_persistent", False)

    @property
    def step_id(self) -> str:
        """ 
        User defined identification string (must be unique within a test tree)
        """
        return getattr(self.data, "step_id", None)

    @property
    def step_tags(self) -> list[str]:
        """ 
        User defined identification tags, can be used to group and identify steps within a test tree
        """
        return getattr(self.data, "step_tags", tuple())

    @property
    def step_duration_estimate(self) -> float:
        """ 
        User defined expected step duration in seconds, helpful for estimating time to complete this step and full tests.
        """
        return getattr(self.data, "step_duration_estimate", 0)

    @property
    def step_is_documented(self) -> bool:
        """ 
        If False then step's documentation will be ignored in Test Procedure Reporting
        """
        return getattr(self.data, "step_is_documented", True)

    @property
    def step_purpose(self) -> str:
        """ 
        Optional override to Step's documented purpose
        """
        return getattr(self.data, "step_purpose", None)

    @property
    def step_triage_refs(self) -> str:
        """ 
        Triage data to help diagnose issues if this Step fails
        """
        return getattr(self.data, "step_triage_refs", None)

    @property
    def step_triage_msg(self) -> str:
        """ 
        Triage text message that appears before any triage reference designators
        """
        return getattr(self.data, "step_triage_msg", None)

    @property
    def step_is_not_applicable(self) -> bool:
        """ 
        If True then this step, not its children will run
        """
        return getattr(self.data, "step_is_not_applicable", False)

    RESULTSTYLE_PASSFAIL = "pass-fail"  # Default resultstyle
    RESULTSTYLE_PASSNA = "pass-n/a"     # Modified result style to fail with N/A rather than FAIL

    @property
    def step_resultstyle(self) -> str:
        """
        How the True-False result of this step should be interpreted.
        Options: "pass-fail" (default), "pass-n/a"
        @see Step.RESULTSTYLE_*
        """
        dattr = getattr(self.data, "step_resultstyle", None)
        if dattr is not None:
            alphas = "".join(re.findall("[a-z]+", dattr.lower()))
            if alphas == "passna":
                return Step.RESULTSTYLE_PASSNA
        return "pass-fail"

    @property
    def step_repeat(self) -> int | bool:
        """
        If not False, step will repeat
        """
        return getattr(self.data, "step_repeat", False)

    @property
    def step_retry(self) -> int | str:
        """If the step should retry upon failure,
            -if a number then it will limit the number of retries
            -if "prompt" then the test operator will be prompted to retry (yes/no)
            -(default) if 0 or None then the test will not retry upon failure
            :rtype: int or str
        """
        return getattr(self.data, "step_retry", self.toplevel.step_retry if self.toplevel else None)

    @property
    def step_teardown_before_retry(self) -> bool:
        """ 
        If True (default) then the step's teardown is run before retrying after a failure
        """
        return getattr(self.data, "step_teardown_before_retry", True)

    @property
    def step_retry_children(self) -> bool | list[bool | str]:
        """Determines if the step should retry upon failure of one of its children

            NOTE: *step_retry_children* is subject to *step_retry* value. Descedent failures are treated as 
            if this step failed, and *step_retry* is consumed (if integer) or the operator is prompted 
            according to the value of *step_retry*

            -if True: then this step will retry its step tree of children if any descendent fails
            -if list[titles...]: then this step will retry its step tree of children if descendents fail that
                are listed in the list of titles. If the first element is a boolean then True=whitelist and 
                False=blacklist of step titles. If no boolean is specified then the list will be a whitelist 
                by default.

            :rtype: bool | tuple(str) | list(str)
        """
        return getattr(self.data, "step_retry_children", False)

    @property
    def step_async_children(self) -> bool:
        """ 
        If True, child steps will be run asynchronously, except for children who's step_force_sync == False
        """
        return getattr(self.data, "step_async_children", False)

    @property
    def step_force_sync(self) -> bool:
        """ 
        If True it prevents siblings from running concurrently with this step
        """
        return getattr(self.data, "step_force_sync", False)

    @property
    def index(self) -> int:
        """ unique numerical test step ID"""
        return self._index

    @property
    def tid(self) -> int:
        """ unique numerical test step ID"""
        return self._index

    @staticmethod
    def _make_selector(query: Any) -> Callable[["Step"], bool]:
        """ convert a wide range of query types into a function that determines a match against a Step instance """
        if type(query) == str:
            if query.startswith("."):
                query = query[1:]
                return lambda s: s.__class__.__name__ == query
            elif query.startswith("@"):
                query = query[1:]
                return lambda s: query in s.step_tags
            else:
                return lambda s: s.title == query
        elif type(query) == int:
            return lambda s: s.tid == query
        elif isinstance(query, type):
            return lambda s: s.__class__ == query
        elif callable(query):
            return query
        else:
            raise SubinitialTestException("Invalid selector: {}".format(query))

    def get_descendant(self, query: str | int | Type | Callable, default: Any = DefaultThrow, include_self=False) -> "Step":
        """
        Get first descendant that matches the selector
        @param query: query string "AStepTitle", ".ClassName", "@TagName", integer TID, ClassTypeObject, callable
        @param default: value to return if query not found
        @param include_self: check if this step matches the query as well as descendants
        @return: child Step if found, default if provided, else throw SubinitialTestException
        """
        selector = self._make_selector(query)
        for step in self._walk():
            if step == self and not include_self:
                continue
            if selector(step):
                return step
        if default is DefaultThrow:
            raise SubinitialTestException("No step found matching query: {}".format(query))
        return default

    def get_descendants(self, query: str | int | Type | Callable, include_self=False) -> list["Step"]:
        """
        Get all descendants that match the selector
        @param query: query string "AStepTitle", ".ClassName", "@TagName", integer TID, ClassTypeObject, callable
        @param include_self: check if this step matches the query as well as descendants
        @return: list of descendent Steps if found, else empty list
        """
        selector = self._make_selector(query)
        found = []
        for step in self._walk():
            if step == self and not include_self:
                continue
            if selector(step):
                found.append(step)
                return step
        return found

    def get_ancestor(self, query: str | int | Type | Callable, default: Any = DefaultThrow) -> "Step":
        """
        Get first ancestor that matches the selector
        @param query: query string "AStepTitle", ".ClassName", "@TagName", integer TID, ClassTypeObject, callable
        @param default: value to return if query not found
        @return: child Step if found, default if provided, else throw SubinitialTestException
        """
        selector = self._make_selector(query)
        step = self.parent
        while step:
            if selector(step):
                return step
            step = step.parent
        if default is DefaultThrow:
            raise SubinitialTestException("No step found matching query: {}".format(query))
        return default

    def has_ancestor(self, step: "Step") -> bool:
        """
        Check if step is an ancestor of this instance
        """
        node = self
        while node.parent is not None:
            if node.parent == step:
                return True
            node = node.parent
        return False
    
    def is_sibling(self, step: "Step") -> bool:
        """
        Check if step is a sibling of this instance
        """
        return step in self.parent.steps

    def __init__(self, **properties):
        self._properties = properties

        self.toplevel: "TestDefinition" = None
        self.reporter: "Reporter" = None
        self.steps = StepCollection(self)
        self.parent: Step = None
        self.assertions: list[AssertionBase] = None
        self.start_time: float = None
        self.procedure_time: float = None
        self.run_time: float = 0
        self.result: bool = None  # The final result of this test Step and all of it's child test Steps.
        self.assertions_result: bool = None
        self.retries = 0

        self.groups: list["StepTable.Group"] = []
        self.requirement_steps: list[Step] = None
        self._index: int = None
        self._assidx = 1
        self._assidx_criteria = 0
        self._depth = 0
        self.stage = self.Stage.PRE

        # async fields
        self.__was_active = False # caches the result of _was_active() for repeated calls
        self._child_idx = 999999  # parent indexes all async running tasks so they can check to see if 
                                      # they're next in line
        self._is_async = False    # parents designate this value if they want their children to run async
        self._thread_step = None  # indicates which thread this step is running on, None = main_thread

        self.data = self._initialize_data(**properties)

        self.title: str = getattr(self.data, "title", None)
        if self.title is None:  # auto generate a title if none is supplied
            self.title = self.generate_title(self.data)  
        
        # self.post_init(self.data)

    def sleep(self, t_sleep: float, tick_rate=1.0, 
            on_tick: Callable[[float, float, float, int], None]=lambda t_sleep, t_elapsed, t_remain, n: None):
        """ 
        Sleep in a way that allows the user to abort the step and not have to wait for the full duration 
        @param t_sleep: duration in seconds for which to sleep
        @param tick_rate: if > 0, then provide a tick to console every tick_rate seconds
        @param on_tick: handler that is called once each tick, and passed t_sleep, elapsed time, remaining time, and n-th tick count
        """
        t_start = time.monotonic()

        if tick_rate <= 0:
            raise ValueError(f"tick_rate must be > 0, not {tick_rate}")
        
        n = 0
        while True:
            
            if self.toplevel._check_for_abort():
                raise KeyboardInterrupt("Abort")

            # calculate time elapsed and time remaining
            t_elapsed = time.monotonic() - t_start
            t_remain = t_sleep - t_elapsed
            
            # handle the end-of-sleep-duration conditions
            if t_remain <= 0:
                return
            elif t_remain <= tick_rate:
                time.sleep(t_remain)
                n += 1
                on_tick(t_sleep, t_sleep, 0, n)
                return
            
            # sleep for a single tick and call the on-tick handler
            else:
                time.sleep(tick_rate)
                n += 1
                on_tick(t_sleep, t_elapsed, t_remain, n)


    def _was_active(self, wait=True):
        if self.__was_active:
            return True
        steps = self.parent.steps
        if steps.cv:
            with steps.cv:
                if steps.active_child_idx < self._child_idx:
                    if wait:
                        steps.cv.wait_for(lambda: steps.active_child_idx >= self._child_idx)
                    else:
                        return False
        self.__was_active = True
        return True

    def _copy(self, idata=None):
        self2 = type(self)(**self._properties)
        return self2

    def _assign_steptablegroups(self):
        for g in sorted(self.groups, key=lambda x: x.priority):
            g.apply(self)

    def _initialize_data(self, **properties) -> Data:
        """
        initialize the class data object if one is defined
        @param properties: kwargs passed to constructor
        """
        # resolve the correct class type for data
        cd = self.__class__.__dict__
        cdata: type = cd.get("Data")
        if cdata:
            data = cdata()  # instantiate a fresh data
        else:
            data = Step.Data()
            cdata = Step.Data
        _freeze_instance_data(data, cdata)

        # add injected properties passed to constructor
        key = ""
        try:
            for key, val in properties.items():
                setattr(data, key, val)
        except AttributeError as ex:
            errmsg = "\nAttributeError: data in {} step has no field: '{}'\n" \
                     "\tAdd '{}' to {}.Data\n\tor check spelling in your constructor in " \
                     "TestDefinition.define_test(...)".format(
                        type(self).__name__, key, key, type(self).__name__)
            print(errmsg)
            sys.exit(1)

        return data

    def post_init(self, data: Data):
        """
        runs at the very end of the Step's constructor (__init__)
        Note: Steps are newly constructed once every test run
        """
        pass

    _titleidx = dict()

    def generate_title(self, data: Data) -> str:
        """
        Assign a title to a Step if no title is supplied by the user.
        Override this to define how the title is assigned by default.

        @return: the Step's title
        """
        steptype = self.__class__.__name__
        idx = Step._titleidx.get(steptype, 0)
        if idx == 0:
            title = steptype
        else:
            title = "{}_{}".format(steptype, idx)
        Step._titleidx[steptype] = idx + 1
        return title

    def __call__(self, *args: "Step") -> "Step":
        """
        Shorthand to add steps as children without having to reference self.add_steps(...)
        @param args: child steps
        @return: reference to self
        """
        self.steps.add(*args)
        return self
    
    def _walk(self) -> Iterator["Step"]:
        yield self
        for s in self.steps:
            for z in s._walk():
                yield z

    def walk(self, include_self=True) -> Iterator["Step"]:
        """ Iterate over the test tree from this step sequentially through its descendants """
        if include_self:
            yield self
        for s in self.steps:
            for z in s._walk():
                yield z

    def add_steps(self, *args: "Step"):
        """ Add child steps to this step """
        for child in args:
            if not isinstance(child, Step):
                raise ValueError("child must be a type derived from Step")
            self.steps.append(child)
            child.parent = self

    def _clear_assertions(self):
        self.assertions = []
        self._assidx = 1

    def add_assertion(self, assertion: AssertionBase):
        """ Add an assertion to this step's PASS/FAIL criteria """
        assertion._index = self._assidx
        if self.stage == self.Stage.PROCEDURE:
            self._assidx_criteria = self._assidx
            assertion.procedural = True
        self._assidx += 1
        self.assertions.append(assertion)

    def assert_true(self, title: str, measurement: bool, **kwargs):
        """
        Specify is True criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        assr = TrueAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_false(self, title: str, measurement: bool, **kwargs):
        """
        Specify is False criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        assr = FalseAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_equal(self, title: str, measurement: Any, standard: Any, **kwargs):
        """
        Specify a == criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        @param standard: expected value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        kwargs['standard'] = standard
        assr = EqualAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_notequal(self, title: str, measurement: Any, standard: Any, **kwargs):
        """
        Specify a != criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        @param standard: rejected value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        kwargs['standard'] = standard
        assr = NotEqualAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_greaterthan(self, title: str, measurement: float, min: float, **kwargs):
        """
        Specify a > criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        @param min: minimum allowed value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        kwargs['min'] = min
        assr = GreaterThanAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_greaterthan_equalto(self, title: str, measurement: float, min: float, **kwargs):
        """
        Specify a ≥ criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        @param min: minimum allowed value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        kwargs['min'] = min
        assr = GreaterThanEqualToAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_lessthan(self, title: str, measurement: float, max: float, **kwargs):
        """
        Specify a < criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        @param max: maximum allowed value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        kwargs['max'] = max
        assr = LessThanAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_lessthan_equalto(self, title: str, measurement: float, max: float, **kwargs):
        """
        Specify a ≤ criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        @param max: maximum allowed value
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        kwargs['max'] = max
        assr = LessThanEqualToAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_tolerance_abs(self, title: str, measurement: float, standard: float, tolerance: float, 
                             negative_tolerance: float | None=None, **kwargs):
        """
        Specify an absolute difference tolerance criteria for a measurement.
        @param title: assertion title
        @param measurement: measured value
        @param standard: expected value under ideal conditions
        @param tolerance: ± absolute tolerance, or + tolerance if negative_tolerance is defined
        @param negative_tolerance: - absolute tolerance
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        if standard is None or tolerance is None:
            pass
        else:
            kwargs['max'] = standard + tolerance
            if negative_tolerance is not None:
                negative_tolerance = abs(negative_tolerance)
                kwargs['min'] = standard - negative_tolerance
            else:
                kwargs['min'] = standard - tolerance
            kwargs['tolerance'] = tolerance
            kwargs['negative_tolerance'] = negative_tolerance
            kwargs['standard'] = standard

        assr = ToleranceAbsAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr 

    def assert_tolerance(self, title: str, measurement: float, standard: float, tolerance: float, 
                         negative_tolerance=None, **kwargs):
        """
        Specify a tolerance criteria for a measurement (e.g. 0.1 == 10%).
        @param title: assertion title
        @param measurement: measured value
        @param standard: expected value under ideal conditions
        @param tolerance: ± ratio tolerance, or + tolerance if negative_tolerance is defined
        @param negative_tolerance: - ratio tolerance, (0.1 == 10%)
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        if standard is None or tolerance is None:
            pass
        else:
            kwargs['max'] = standard * (1+tolerance)
            if negative_tolerance is not None:
                negative_tolerance = abs(negative_tolerance)
                kwargs['min'] = standard * (1-negative_tolerance)
            else:
                kwargs['min'] = standard * (1-tolerance)
            kwargs['tolerance'] = tolerance
            kwargs['negative_tolerance'] = negative_tolerance
            kwargs['standard'] = standard

            if standard < 0:
                max_ = kwargs['min']
                kwargs['min'] = kwargs['max']
                kwargs['max'] = max_

        assr = ToleranceAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_inrange(self, title, measurement, 
                       min: float | None=None, 
                       max: float | None=None, 
                       range: tuple[float, float] | None=None, 
                       **kwargs):
        """
        Specify a range criteria for a measurement (e.g. Min ≤ x ≤ Max).
        @param title: assertion title
        @param measurement: measured value
        @param min: minimum allowed value (unless range is specified)
        @param max: maximum allowed value (unless range is specified)
        @param range: [min, max] (takes priority over min/max parameters)
        
        ## Extra Parameters:
            - precision (int): floating point precision of measurement
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        """ Assert that a measurement is within (inclusive) min and max limits """
        kwargs['min'] = min
        kwargs['max'] = max
        kwargs['range'] = range
        assr = InRangeAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def assert_record(self, title: str, measurement: Any, **kwargs):
        """ 
        Add an assertion that simply records a data measurement, no pass/fail criteria 
        @param title: assertion title
        @param measurement: measured value
        
        ## Extra Parameters:
            - units     (str): measurement units, e.g. 'V', 'A', 'Ω'
            - tag       (int): tag for referencing assertion in documentation comments
        """
        assr = RecordAssertion(title, measurement, **kwargs)
        self.add_assertion(assr)
        self.report_assertion(assr)
        return assr

    def prompt_fields(self, *fields):
        """ DEPRECATED """
        r = self.reporter
        if r:
            return r.prompt_fields(*fields, step=self)
        return None

    def prompt_input(self, prompt: str, title: str | None=None, markdown=False) -> str | None:
        """ 
        Prompt the operator for some textual input 
        @param title: set modal window title in TestCenter, or print a title line in console.
        @param markdown: render prompt as markdown in TestCenter, (ignored in concole)
        @return: the operator's entry
        """
        r = self.reporter
        if r:
            return r.prompt_input(prompt, step=self, title=title, markdown=markdown)
        return None

    def prompt_continue(self, prompt: str, can_cancel=False, title: str | None=None, markdown=False, **kwargs) -> bool:
        """ 
        Prompt the operator before continuing forward with the test 
        @param can_cancel: allow the operator to select a negative response (or click out of the modal in TestCenter)
        @param title: set modal window title in TestCenter, or print a title line in console.
        @param markdown: render prompt as markdown in TestCenter, (ignored in concole)
        @return: True of continue, False if cancelled
        """
        can_cancel = can_cancel or kwargs.get("cancel", False)  # deprecated 'cancel' vs can_cancel nomenclature
        r = self.reporter
        if r:
            return r.prompt_continue(prompt, can_cancel=can_cancel, title=title, step=self, markdown=markdown)

    def report_info(self, *infotext: Any, clear=False):
        """ Report some textual information to display neatly with test output """
        r = self.reporter
        if r:
            r.report_info(" ".join(str(x) for x in infotext), step=self, clear=clear)

    def report_assertion(self, assertion: AssertionBase):
        """ Report an assetion object """
        r = self.reporter
        if r:
            r.report_assertions(self, assertion)

    def report_error(self, error: Exception | str, traceback=""):
        """ Report a caught exception and optional traceback to display neatly within test output """
        r = self.reporter
        if r:
            r.report_error(error, traceback=traceback, step=self)

    def _get_flagslist(self) -> str:
        flags = []
        if self.toplevel.final == self:
            flags.append("Final")
        if self.step_is_requirement:
            flags.append("Requirement")
        if self.step_is_persistent:
            flags.append("Persistent")
        if self.step_is_resilient:
            flags.append("Resilient")
        if self.step_is_independent:
            flags.append("Independent")
        if self.step_resultstyle == Step.RESULTSTYLE_PASSNA:
            flags.append("Pass:N/A")
        if self._is_async:
            flags.append("Sync" if self.step_force_sync else "Async")
        return flags

    def _decorate_title(self, titleprefix="", flagsprefix="") -> str:
        flags = self._get_flagslist()
        if len(flags) > 0:
            return "{}{} -{}".format(titleprefix, self.title, flagsprefix + " -".join(flags))
        else:
            return titleprefix + str(self.title)

    def _get_depth(self) -> int:
        return self._depth

    def criteria(self, data: Data):
        """
        Define pass/fail criteria for this Step
        :rtype: Boolean
        @returns: return False explicitly if substeps should not be run, or True if they should be run. If return value
            is omitted the framework will determine if substeps should be run automatically based on assertion pass/fail
            Return  :   Action
            None    :   Default
            True    :   PASS
            False   :   FAIL
            "SKIP"  :   SKIP
            "N/A"   :   N/A      (Not Applicable, test and children greyed out)
        """
        return True

    def procedure(self, data: Data):
        """ Perform the step's test procedure
        """
        return None

    def teardown(self, data: Data):
        """Teardown anything setup by code, after all substeps are run if any, even if the procedure or substeps crash"""
        pass

    def log(self, *args):
        pass  # logger.info(*args)

    def _find_failed_descendents(self):
        return list(s for s in self._walk() if s.result is False)

    def _should_retry_children(self, single_stepping):
        if not self._should_retry(single_stepping, children=True):
            return False

        retriable_steps = self.step_retry_children
        if not retriable_steps:
            return False
        if type(retriable_steps) not in (list, tuple):
            return True

        whitelist = True
        if type(retriable_steps[0]) == bool:
            whitelist = retriable_steps[0]
            retriable_steps = retriable_steps[1:]
        titles = []
        typenames = []
        for selector in retriable_steps:
            if selector.startswith("."):
                typenames.append(selector[1:])
            else:
                titles.append(selector)

        for failed_step in self._find_failed_descendents():
            if whitelist:
                if (failed_step.title not in titles) and \
                        (failed_step.__class__.__name__ not in typenames):
                    return False
            else:
                if (failed_step.title in titles) or \
                        (failed_step.__class__.__name__ in typenames):
                    return False
        return True

    def _should_retry(self, single_stepping, children=False):
        if self.toplevel._abort:
            return False

        do_retry = False
        step_retry = self.step_retry

        if type(step_retry) == bool and step_retry:
            step_retry = 1

        if children:
            prompt_msg = "TID {:03} {}'s child step(s): failed, retrying".format(self.index, self.title)
        else:
            prompt_msg = "TID {:03} {}: failed, retrying".format(self.index, self.title)

        prompt_retry = type(step_retry) == str and step_retry.strip().lower() == "prompt"
        if single_stepping or prompt_retry:
            if self.prompt_continue(prompt_msg, cancel=True):
                do_retry = True

        elif type(step_retry) == int and self.retries < step_retry:
            do_retry = True

        if do_retry:
            self.retries += 1
            self.report_info("{}. Retry #{}".format(prompt_msg, self.retries))
            return True
        return False

    def _register_requirement_step(self, step):
        if self.requirement_steps is None:
            self.requirement_steps = [step]
        elif step not in self.requirement_steps:
            self.requirement_steps.append(step)

    def _run(self, toplevel: "TestDefinition", is_mock: bool):
        """
        Runs the test node and any child test nodes if possible
        """
        self.log("%s._run entered...", self.title)
        if toplevel._abort:
            self.log("%s_run -> abort", self.title)
            return False
        children_were_run = False

        reporter = self.reporter = toplevel.reporter
        self.start_time = time.time()

        if self.skip or self._index in toplevel.skippoints:
            self.log("%s._run -> skip", self.title)
            self._clear_assertions()
            self.result = None
            reporter.report_stepstart(self)
            reporter.report_criteria(self)
            reporter.report_stepresult(self)
            return True

        if toplevel.target:
            if toplevel.target == self:
                toplevel.target = None
            elif not toplevel.target.has_ancestor(self) and not self.step_is_requirement:
                self._clear_assertions()
                self.result = None
                self.procedure_time = 0
                self.run_time = 0
                self.stage = self.Stage.POST
                reporter.report_stepstart(self)
                reporter.report_criteria(self)
                reporter.report_stepresult(self)
                return self.result

        if self.step_is_not_applicable:
            self.log("%s._run step_is_not_applicable", self.title)
            self._clear_assertions()
            self.result = "N/A"
            self.procedure_time = 0
            self.run_time = 0  # time.time() - self.start_time
            self.stage = self.Stage.POST
            reporter.report_stepresult(self)
            return self.result

        if self.step_is_persistent:
            toplevel._register_persistent_step(self)
        elif self.step_is_requirement:
            self.parent._register_requirement_step(self)
        

        try:
            while True:
                self.log("%s._run procedure()", self.title)
                if reporter.report_stepstart(self):
                    return False
                single_stepping = toplevel.state == 'paused'

                self._clear_assertions()
                self.procedure_time = None

                p_tstart = time.time()
                self.stage = self.Stage.PROCEDURE
                if is_mock:
                    self.mock_procedure(self.data)
                else:
                    self.procedure(self.data)

                self.log("%s._run criteria()", self.title)
                self.stage = self.Stage.CRITERIA
                self.result = self.criteria(self.data)
                self.stage = self.Stage.POST
                self.procedure_time = time.time() - p_tstart

                self.log("%s._run result -> %s", self.title, self.result)
                # auto detect pass/fail from assertions
                if self.result is None:
                    self.result = True
                    for assr in self.assertions:
                        if assr.result is False:
                            self.result = False
                            break
                if self.step_resultstyle == self.RESULTSTYLE_PASSNA:
                    if self.result is False:
                        self.result = "N/A"
                self.assertions_result = self.result

                reporter.report_criteria(self)

                if self.result is False:
                    try:
                        self.toplevel.on_step_failure(self)
                    except Exception as ex:
                        self.report_error("Error on_step_failure(): {}".format(ex))

                # evaluate if we should break on an assertion failing
                if self.result is False and toplevel.stop_on_assertion_fail:
                    for assr in self.assertions:
                        if assr.result is False:
                            msg = "{}: {} assertion failed, tearing down".format(self.title, assr.title)
                            sys.stderr.write(msg)
                            raise SubinitialTestException(msg)

                if self.result is False and (self.step_retry or single_stepping):
                    if self._should_retry(single_stepping):
                        if self.step_teardown_before_retry:
                            self.teardown(self.data)
                        continue

                # run the child testnodes
                self.log("%s._run run children", self.title)
                if ((self.result is True) or self.children_are_independent) and len(self.steps):
                    children_were_run = True
                    # toplevel._rundepth += 1

                    if self.step_async_children:
                        cv = self.steps.cv = threading.Condition()
                        for async_steps in self.steps.async_groups():
                            self.steps.active_child_idx = -1
                            tasks = []
                            for idx, step in enumerate(async_steps):
                                step._is_async = True
                                step._thread_step = step
                                step._child_idx = idx
                                t = Task(step._run, toplevel, is_mock)
                                tasks.append((t, step))
                            for (t, step) in tasks:
                                with cv:
                                    self.steps.active_child_idx = step._child_idx
                                    cv.notify_all()
                                chresult = t.wait(timeout=None)
                                self.reporter.report_stepfinishedasync(step)
                                if chresult is False or chresult == "EXIT":
                                    if not self.step_is_independent:
                                        self.result = "EXIT"
                    else:
                        for child in self.steps:
                            self.steps.active_child = child
                            child._thread_step = self._thread_step
                            chresult = child._run(toplevel, is_mock)
                            if toplevel._final_reached or toplevel.final == child:
                                toplevel._final_reached = True
                                return
                            if chresult is False or chresult == "EXIT":
                                if not self.step_is_independent:
                                    self.result = "EXIT"
                                if toplevel._abort or child.step_is_requirement or not self.step_is_resilient:
                                    return
                    if self.result == "EXIT" and not toplevel._abort:
                        if self._should_retry_children(single_stepping):
                            self._teardown(toplevel, is_mock, requirements_only=True)
                            continue
                
                if self.step_repeat:
                    continue

                break  # while True should never run unless retry event calls continue

        except (Exception, KeyboardInterrupt) as e:
            self.log("%s._run exception", self.title, e)
            self.stage = self.Stage.ERROR
            if not self.procedure_time:
                self.procedure_time = time.time() - self.start_time
            if type(e) == KeyboardInterrupt:
                toplevel._abort = True
            else:
                trace = traceback.format_exc() if toplevel.print_traceback else None
                self.report_error(e, trace)
            self.result = False

        finally:
            self.log("%s._run finally", self.title)
            self._teardown(toplevel, is_mock)
            self.run_time = time.time() - self.start_time
            reporter.report_stepresult(self)
            return self.result

    def _teardown(self, toplevel: "TestDefinition", is_mock: bool, requirements_only=False):
        if not is_mock:
            steps: list[Step] = []
            if self.requirement_steps:
                steps = list(reversed(self.requirement_steps))
            if not requirements_only and not self.step_is_persistent and not self.step_is_requirement:
                steps = [self] + steps
            self.requirement_steps = []
            for step in steps:
                try:
                    step.teardown(step.data)
                except (Exception, KeyboardInterrupt) as e:
                    if type(e) == KeyboardInterrupt:
                        toplevel._abort = True
                    else:
                        toplevel._abort = True
                        self.report_info(f"in teardown() for {step.title}:")
                        trace = traceback.format_exc() if toplevel.print_traceback else None
                        self.report_error(e, trace)

    def _get_structure(self, init=False):
        steps = []
        assers = [] if self.result == "N/A" else list(a.to_dict(init) for a in self.assertions)

        node = {"title": self.title, "assertions": assers,
                "flags": self._get_flagslist(),
                "skip": self.skip,
                "duration_estimate": self.step_duration_estimate,
                "repeat": self.step_repeat,
                "steps": steps, "result": AssertionBase.get_resultset(init, self.result)}

        for step in self.steps:
            cnode = step._get_structure(init)
            steps.append(cnode)
        return node

    def mock_procedure(self, data: Data):
        pass

    def _mock_evaluate(self, toplevel: "TestDefinition"):
        self.toplevel = toplevel
        self._clear_assertions()
        if self.step_is_not_applicable:
            # drill down and set to N/A
            self.result = "N/A"
            for step in self.steps:
                step._drill_na()
        self.criteria(self.data)


class StepNA(Step):
    """
    Helper Step class. Inherit from this if you automatically want your criteria failure to be understood
    as N/A (step's children will be ignored intentionally because they are presumed not-applicable)
    """
    class Data:
        def __init__(data):
            data.applicable = False

    def criteria(self, data: Data):
        self.assert_true("Applicable", data.applicable)

    @property
    def step_resultstyle(self):
        return Step.RESULTSTYLE_PASSNA
