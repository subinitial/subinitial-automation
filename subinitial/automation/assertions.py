# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework assertions
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

class AssertionBase:
    @staticmethod
    def get_resultset(isinit, result):
        if result == "N/A":
            return "N/A", "N/A", "na"
        if isinit:
            return None, "-", ""
        if result is None:
            return None, "SKIP", "skip"
        if result is True:
            return True, "PASS", "pass"
        if result is False:
            return False, "FAIL", "fail"
        return result, "", ""

    default_precision = 6

    def __init__(self, title: str, measurement, **kwargs):
        self.title: str = title
        self.measurement = measurement
        self.criteria: str = kwargs.get('criteria', None)
        self.result: bool = kwargs.get('result', None)
        self.css_classes: str = kwargs.get('css_classes', None)
        self.procedural = False
        self.precision: int = kwargs.get('precision', self.default_precision)
        if type(self.precision) == str:
            self._precision = self.precision
        else:
            self._precision = "{:." + str(self.precision) + "g}"
        self.units: str = kwargs.get('units', None)
        self._units: str = " " + self.units if self.units is not None else ""
        self.passtext = "PASS"
        self.failtext = "FAIL"
        self.tag: str = kwargs.get('tag', None)
        if self.tag:
            self.tag = str(self.tag)
        self._index = 0
        self.evaluate(**kwargs)

    @property
    def index(self) -> int:
        return self._index

    def _structured_criteria(self) -> str:
        return self.criteria

    def to_dict(self, isinit):
        r = dict(aid=self._index, type=type(self).__name__, parameter=self.title,
                 criteria=self._structured_criteria(),
                 procedural=self.procedural,
                 min=getattr(self, "min", None),
                 max=getattr(self, "max", None))
        r["result"], r["_result"], r["resultclass"] = self.get_resultset(isinit, self.result)
        r["measurement"], r["_measurement"] = (self.measurement, self._measurement_tostr()) if not isinit else (None, "-")
        if self.css_classes:
            r["css_classes"] = self.css_classes
        if self.passtext != "PASS":
            r["passtext"] = self.passtext
        if self.failtext != "FAIL":
            r["failtext"] = self.failtext
        if self.units:
            r["units"] = self.units
        return r

    def _measurement_tostr(self) -> str:
        if type(self.measurement) == float:
            return self._precision.format(self.measurement)
        elif self.measurement is None:
            return "None"
        else:
            return str(self.measurement)

    def __str__(self) -> str:
        name = self.title + ", " if self.title is not None else ""
        result = self.passtext if self.result else self.failtext
        if self.units is not None:
            return "Testing: {}Criteria: {}, Measurement: {} {}, Result: {}" \
                .format(name, self.criteria, self._measurement_tostr(), self.units, result)
        return "Testing: {}Criteria: {}, Measurement: {}, Result: {}"\
            .format(name, self.criteria, self._measurement_tostr(), result)

    def evaluate(self, **kwargs):
        pass


class TrueAssertion(AssertionBase):
    def evaluate(self, **kwargs):
        if self.criteria is None:
            self.criteria = "x is True"
        self.result = self.measurement is True


class FalseAssertion(AssertionBase):
    def evaluate(self, **kwargs):
        if self.criteria is None:
            self.criteria = "x is False"
        self.result = self.measurement is False


class EqualAssertion(AssertionBase):
    def evaluate(self, **kwargs):
        self.standard = kwargs.get('standard', None)
        if self.standard is None:
            self.criteria = self.criteria or "x = None"
        elif self.criteria is None:
            try:
                self.criteria = "x = {}".format(self._precision).format(self.standard)
            except (ValueError, TypeError):
                self.criteria = "x = {}".format(self.standard)
        self.result = self.measurement == self.standard


class NotEqualAssertion(AssertionBase):
    def evaluate(self, **kwargs):
        self.standard = kwargs.get('standard', None)
        if self.standard is None:
            self.criteria = self.criteria or "x != None"
        elif self.criteria is None:
            try:
                self.criteria = "x != {}".format(self._precision).format(self.standard)
            except ValueError:
                self.criteria = "x != {}".format(self.standard)
        self.result = self.measurement != self.standard


class GreaterThanAssertion(AssertionBase):
    def _structured_criteria(self):
        return "Min < x"

    def evaluate(self, **kwargs):
        self.min = kwargs.get('min', None)
        if self.min is None:
            self.criteria = self.criteria or "x > ?"
            self.result = False
            return
        elif self.criteria is None:
            self.criteria = "x > {}".format(self._precision).format(self.min)
        if self.measurement is None:
            self.result = False
            return
        self.result = self.measurement > self.min


class GreaterThanEqualToAssertion(AssertionBase):
    def _structured_criteria(self):
        return "Min ≤ x"

    def evaluate(self, **kwargs):
        self.min = kwargs.get('min', None)
        if self.min is None:
            self.criteria = self.criteria or "x ≥ ?"
            self.result = False
            return
        if self.criteria is None:
            self.criteria = "x ≥ {}".format(self._precision).format(self.min)
        if self.measurement is None:
            self.result = False
            return
        self.result = self.measurement >= self.min


class LessThanAssertion(AssertionBase):
    def _structured_criteria(self):
        return "x < Max"

    def evaluate(self, **kwargs):
        self.max = kwargs.get('max', None)
        if self.max is None:
            self.criteria = self.criteria or "x < ?"
            self.result = False
            return
        if self.criteria is None:
            self.criteria = "x < {}".format(self._precision).format(self.max)
        if self.measurement is None:
            self.result = False
            return
        self.result = self.measurement < self.max


class LessThanEqualToAssertion(AssertionBase):
    def _structured_criteria(self):
        return "x ≤ Max"

    def evaluate(self, **kwargs):
        self.max = kwargs.get('max', None)
        if self.max is None:
            self.criteria = self.criteria or "x ≤ ?"
            self.result = False
            return
        if self.criteria is None:
            self.criteria = "x ≤ {}".format(self._precision).format(self.max)
        if self.measurement is None:
            self.result = False
            return
        self.result = self.measurement <= self.max


class ToleranceAbsAssertion(AssertionBase):
    def evaluate(self, **kwargs):
        self.tolerance = abs(kwargs.get('tolerance', 0))
        self.tolerance_precision = kwargs.get('tolerance_precision', 3)

        negative_tolerance = kwargs.get('negative_tolerance', None)
        if negative_tolerance is not None:
            self.negative_tolerance = abs(negative_tolerance)
        else:
            self.negative_tolerance = None

        self.standard = kwargs.get('standard', None)
        self.max = kwargs.get('max', None)
        self.min = kwargs.get('min', None)

        if None in (self.max, self.min, self.standard, self.tolerance):
            self.criteria = "? ±? (? ≤ x ≤ ?)"
            self.result = False
            return

        if self.criteria is None:
            if self.negative_tolerance is not None:
                tol = "+{0}/-{0}".format(self._precision).format(self.tolerance, self.negative_tolerance)
            else:
                tol = "±{0}".format(self._precision).format(self.tolerance)
            self.criteria = "{0} {{}} ({{:.4g}} ≤ x ≤ {{:.4g}})".format(self._precision).format(self.standard, tol, self.min, self.max)
        if self.measurement is None:
            self.result = False
            return
        self.result = self.min <= self.measurement <= self.max


class ToleranceAssertion(AssertionBase):
    def evaluate(self, **kwargs):
        self.tolerance = abs(kwargs.get('tolerance', 0))
        self.tolerance_precision = kwargs.get('tolerance_precision', 3)

        negative_tolerance = kwargs.get('negative_tolerance', None)
        if negative_tolerance is not None:
            self.negative_tolerance = abs(negative_tolerance)
        else:
            self.negative_tolerance = None

        self.standard = kwargs.get('standard', None)
        self.max = kwargs.get('max', None)
        self.min = kwargs.get('min', None)

        if None in (self.max, self.min, self.standard, self.tolerance):
            self.criteria = "? ±?% (? ≤ x ≤ ?)"
            self.result = False
            return

        if self.criteria is None:
            if self.negative_tolerance is not None:
                tol = "+{0}/-{0}%".format(self._precision).format(self.tolerance*100, self.negative_tolerance*100)
            else:
                tol = "±{0}%".format(self._precision).format(self.tolerance*100)
            self.criteria = "{0} {{}} ({{:.4g}} ≤ x ≤ {{:.4g}})".format(self._precision).format(self.standard, tol, self.min, self.max)
        if self.measurement is None:
            self.result = False
            return
        self.result = self.min <= self.measurement <= self.max

    def _structured_criteria(self):
        return self.criteria.split('(')[0][:-1]

    def _measurement_tostr(self):
        return "{} ({})".format(AssertionBase._measurement_tostr(self), self._measured_tolerance_str())

    def _measured_tolerance_str(self):
        if None in (self.measurement, self.standard, self.tolerance_precision):
            return "?%"
        actual = (self.measurement - self.standard) / self.standard * 100

        precision = "{:." + str(self.tolerance_precision) + "g}"
        if actual > 0:
            return "+" + precision.format(actual) + "%"
        else:
            return precision.format(actual) + "%"


class InRangeAssertion(AssertionBase):
    def _structured_criteria(self):
        return "Min ≤ x ≤ Max"

    def evaluate(self, **kwargs):
        rng = kwargs.get('range', None)
        if rng is not None:
            self.min, self.max = rng
        else:
            self.min = kwargs.get('min', None)
            self.max = kwargs.get('max', None)
        if self.min is None or self.max is None:
            self.result = False
            self.criteria = self.criteria or "? ≤ x ≤ ?"
            return
        if self.criteria is None:
            self.criteria = "{0} ≤ x ≤ {0}".format(self._precision).format(self.min, self.max)
        if self.measurement is None:
            self.result = False
            return
        self.result = self.min <= self.measurement <= self.max


class RecordAssertion(AssertionBase):
    def _structured_criteria(self):
        return "Record"

    def evaluate(self, **kwargs):
        if self.criteria is None:
            self.criteria = "record"
        self.result = ""

    def __str__(self):
        name = self.title + ", " if self.title is not None else ""
        if self.units:
            return "Recording: {}Measurement: {} {}" \
                .format(name, self._measurement_tostr(), self.units)
        return "Recording: {}Measurement: {}" \
            .format(name, self._measurement_tostr())
