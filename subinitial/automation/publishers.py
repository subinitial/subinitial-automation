# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework test results publishers
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from .utils import NodeString


class Publisher:
    _result_to_str = {
        True: "PASS",
        None: "-",
        False: "FAIL"
    }

    def generate(self, **kwargs):
        pass


class CsvPublisher(Publisher):
    def __init__(self, testtree, filename):
        self.testnode = testtree
        self.filename = str(filename)
        self.drawtree = False

    def generate(self):
        table = list()
        atp = self.testnode

        report = atp.report

        # append title
        table.append([report.title])

        # append fields
        table.append([])  # blank space
        for key, field in self.testnode.fields.items():
            table.append([field.title, field.entry])
        table.append(['Duration', round(atp.run_time, 3), 's'])

        # results
        table.append([])  # blank space
        table.append([report.passcount, "TESTS PASSED"])
        # table.append([report.skipcount, "TESTS SKIPPED"])
        table.append([report.failcount, "TESTS FAILED"])
        table.append([report.assertion_failcount, "ASSERTIONS FAILED"])

        # append headers
        table.append([])  # blank space
        table.append(['TID:A#', 'Test Steps / Assertions', 'Criteria', 'Min', 'Measurement', 'Max', 'Units', 'Result', 'Step-Time', 'Procedure-Time', 'Notes'])

        # append procedure tree rows
        for step in self.testnode.steps:
            self.append_treerows(step, table)

        # last tree row
        if self.drawtree:
            table[-1][1] = table[-1][1].replace(NodeString.treefill, NodeString.lastchildglyph)

        # serialize to CSV
        from .utils import CsvBuilder
        csvb = CsvBuilder(self.filename)
        csvb.write_table(table)

        if hasattr(self.testnode, "reporter") and self.testnode.reporter:
            self.testnode.reporter.report_info("CSV Report Generated: {}".format(self.filename))

    def append_treerows(self, step, table):
        # append a row for the test title and result
        nodestr = NodeString.node(step._get_depth(), step._decorate_title()) if self.drawtree \
            else step._decorate_title()

        notes = ""
        if step.retries:
            notes = "retries: {}".format(step.retries)
        table.append([step._index, nodestr,
                      '', '', '', '', '', self._result_to_str.get(step.result, step.result),
                      round(step.run_time, 3) if step.run_time is not None else '-',
                      round(step.procedure_time, 3) if step.procedure_time is not None else '-',
                      notes])

        # append assertion rows if they exist
        if step.result != "N/A" and step.result is not None:
            if step.assertions is not None and len(step.assertions):
                step_depth = step._get_depth()

                def add_assertion(assr, islast=False):
                    nodestr_ = NodeString.child(step_depth, assr.title, islast) if self.drawtree \
                        else assr.title
                    table.append(["{}::{}".format(step.index, assr.index),
                                  nodestr_, assr._structured_criteria(),
                                  getattr(assr, "min", ""), assr.measurement, getattr(assr, "max", ""), assr.units,
                                  self._result_to_str.get(assr.result, assr.result)])
                lastkey = None
                for assr in step.assertions[:-1]:
                    add_assertion(assr)
                # table.pop()
                add_assertion(step.assertions[-1], islast=True)

        #recursion
        for child in step.steps:
            self.append_treerows(child, table)
