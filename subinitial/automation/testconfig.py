# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework Config .CSV code
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.
from typing import Callable, Iterable, Any, TYPE_CHECKING, Iterator, TypeAlias, Union, TypeVar, Type
import csv
import sys
import os
import glob
from pathlib import Path
import json
from .utils import text_eval, argsC, argsF
from .parameter import Parameter


if TYPE_CHECKING:
    from .step import Step


T = TypeVar('T')
EMPTY_DICT = None

_locate_cache: dict[str, 'TestDirectory'] = {}
_testconfig_cache = {}
_teststate_cache = {}


Cell:    TypeAlias = str | int | float | bool | list | dict
Row:     TypeAlias = list[Cell]
RowDict: TypeAlias = dict[str, Cell]


class GenericTable(list[Row]):
    """
    A generic CSV table schema
        - The first row should be headers where each columnm specified a header title string
        - Each subsequent row is a list of cell values added to this GenericTable
    """
    
    def __init__(self, cfg):
        list.__init__(self)
        self.name = ""
        self.file: 'ConfigFile' = cfg
        self.headers: list[str] = []
        self._to_filerow: list[int] = []
        self._ser = None

    def serialize(self, cache=True) -> dict[str, Any]:
        if not cache:
            self._ser = None
        if self._ser is None:
            self._ser = dict(headers=self.headers, data=self, type="GenericTable", name=self.name)
        return self._ser

    def _load(self, reader, priority):
        line_num, row = _next_row(self.file, reader)
        if row is False: return priority
        self.headers = list(x for x in row if x != "")
        hlen = len(self.headers)
        while True:
            line_num, row = _next_row(self.file, reader)
            while len(row) < hlen:
                row.append(None)
            if row is False: return priority
            self.append(list(text_eval(x) for x in row[:hlen]))
            self._to_filerow.append(line_num)
        return priority

    def update(row_num: int, col_num: int, value: str):
        pass

    def find(self, where: dict[str, Any], as_dict=True) -> None | list[Any] | dict[str, Any]:
        """
        Find the first row where key:values in where match
        @param as_dict: if True, return the row as a dictionary keyed by its header column names
        """
        iwhere = {}
        for i, header in enumerate(self.headers):
            if header in where:
                iwhere[i] = where[header]
        for row in self:
            if all(iwhere[i] == row[i] for i in iwhere):
                if as_dict:
                    return {k: v for k, v in zip(self.headers, row)}
                else:
                    return row
        return None


class StepTable:
    """ A StepTable read from a config.csv style table """

    class Group:
        CATEGORY_TITLE = 0
        CATEGORY_TAG = 1
        CATEGORY_TYPE = 2

        type_test = lambda node, criteria: node.__class__.__name__ == criteria
        tag_test = lambda node, criteria: criteria in node.step_tags if node.step_tags else False
        title_test = lambda node, criteria: criteria == node.title
        test_functions = title_test, tag_test, type_test

        priorities = [10000000, 10000, 10]

        class ChildSelector:
            """ The '>' selects direct children who match the query """
            def __init__(self, test: Callable[["Step", Any], bool], value: Any):
                self.criteria = test, value
                self.fieldvals: list[tuple[str, Any]] = []  # list of tuples

            def assign(self, node: "Step", group: 'StepTable.Group'):
                test, value = self.criteria
                for child in node.steps:
                    if test(child, value):
                        child.groups.append(group)

        class DescendantSelector:
            """ The '>>' selects all descendents (children and children of children recursively) who match the query """
            def __init__(self, test: Callable[["Step", Any], bool], value: Any):
                self.criteria = test, value
                self.fieldvals: list[tuple[str, Any]] = []  # list of tuples

            def assign(self, node: "Step", group: 'StepTable.Group'):
                test, value = self.criteria
                for child in node._walk():
                    if test(child, value):
                        child.groups.append(group)

        def assign(self, node: "Step"):
            if self.subselector:
                self.subselector.assign(node, self)
            else:
                node.groups.append(self)

        def apply(self, node: "Step"):
            for field, val in self.fieldvals:
                if field.lower() == "skip":
                    if val is None:
                        val = True
                    setattr(node.data, "skip", val)
                else:
                    setattr(node.data, field, val)

        def __init__(self, selector: str, starting_priority: int):
            self.selector = selector
            self.fieldvals: list[tuple[str, Any]] = []
            self.priority: int = starting_priority
            self.category: int = None
            self.entry: str = None
            self.subselector:  Union["StepTable.Group.ChildSelector", "StepTable.Group.DescendantSelector"] = None
            self.compile_selector()

        def add_fieldval(self, field: str, val: Any):
            self.fieldvals.append((field, val))

        @classmethod
        def _parse_entry(cls, selector: str):
            selector = selector.strip()
            l = len(selector)
            i = 0

            delimiters = (',', '>')
            c = selector[i]

            entry = None
            category = cls.CATEGORY_TITLE
            quotes = False
            escape = False
            if c == '.':
                category = cls.CATEGORY_TYPE
                i += 1
            elif c == '@':
                category = cls.CATEGORY_TAG
                i += 1
            if selector[i] == '"':
                quotes = True
                i += 1

            start = i
            while True:
                c = selector[i]
                if quotes:
                    if c == '\\':  # escaped quotes
                        escape = True
                        i = min(i + 2, l)
                        continue
                    elif c == '"':  # end quotes
                        entry = selector[start:i]
                        if escape:
                            entry = entry.replace('\\"', '"')
                        i += 1
                        break
                else:
                    if c in delimiters:
                        break

                i += 1
                if i >= l:
                    break

            if not entry:
                entry = selector[start:i].strip()

            while True:
                if i >= l:
                    i = 0
                if not selector[i].isspace():
                    break
                i += 1

            return category, entry, i

        def compile_selector(self):
            category, entry, next = self._parse_entry(self.selector)
            self.category = category
            self.entry = entry
            self.priority += self.priorities[category]

            if next:
                selectortext = self.selector[next:]
                if selectortext.startswith(">>"):
                    selector_type = self.DescendantSelector
                    selectortext = selectortext[2:]
                elif selectortext.startswith(">"):
                    selector_type = self.ChildSelector
                    selectortext = selectortext[1:]
                else:
                    raise Exception("StepTable: invalid selector: {}".format(selectortext))

                scategory, sentry, snext = self._parse_entry(selectortext)
                if scategory is None or snext:
                    raise Exception("StepTable: invalid selector: {}".format(selectortext))

                self.priority += self.priorities[scategory]
                self.subselector = selector_type(self.test_functions[scategory], sentry)

    def __init__(self, cfg):
        self.file: 'ConfigFile' = cfg
        self.groups: list[StepTable.Group] = []

        self.titles: dict[str, list[StepTable.Group]] = dict()
        self.types: dict[str, list[StepTable.Group]] = dict()
        self.tags: dict[str, list[StepTable.Group]] = dict()

        self.categories = self.titles, self.types, self.tags
        self.name = ""
        self._dict = None

    def serialize(self):
        if self._dict is None:
            self._dict = dict(
                groups=tuple((x.selector, x.fieldvals) for x in self.groups),
                type="StepTable",
                name=self.name
            )
        return self._dict

    def _load(self, reader, priority):
        self.groups = []
        lastgroup: StepTable.Group = None
        while True:
            line, row = _next_row(self.file, reader)
            if row is False:
                break

            selector: str = row[0]
            if len(selector) == 0 or selector.isspace():
                if lastgroup is None:
                    raise Exception("StepTable parse error: {}".format(row))
                group: StepTable.Group = lastgroup
            else:
                group: StepTable.Group = StepTable.Group(selector.strip(), priority)
                self.groups.append(group)
                lastgroup = group
                priority += 1

            field = row[1]
            # parse value
            val: Any = []
            for x in row[2:]:
                if len(x) == 0 or x[0] == '#':
                    break
                val.append(text_eval(x))
            # contract 1-length tuples
            while len(val) > 1 and val[-1] is None:
                val.pop()
            if len(val) == 1:
                val = val[0]

            group.add_fieldval(field, val)

            catdict = self.categories[group.category]
            catdictent = catdict.get(group.entry)
            if catdictent is None:
                catdict[group.entry] = [group]
            else:
                catdictent.append(group)

        return priority

    def _add(self, st: 'StepTable'):
        for dst, src in (
                (self.titles, st.titles),
                (self.tags, st.tags),
                (self.types, st.types)
        ):
            for k, v in src.items():
                vdst = dst.get(k, None)
                if vdst is None:
                    dst[k] = v
                else:
                    vdst += v


    @staticmethod
    def _combine(cfg, steptables: Iterable['StepTable']) -> 'StepTable':
        if type(steptables) == StepTable:
            return steptables
        elif len(steptables) == 0:
            return StepTable(cfg)
        elif len(steptables) == 1:
            return steptables[0]

        st = StepTable(cfg)
        for stx in steptables:
            st._add(stx)

        return st

    def assign(self, node: "Step", walkparam=None):
        by_title, by_tag, by_type = self.categories
        empty = tuple()
        for step in node._walk(walkparam):
            groups = by_type.get(step.__class__.__name__, empty)
            for group in groups:
                group.assign(step)

            if step.step_tags:
                for tag in step.step_tags:
                    groups = by_tag.get(tag, empty)
                    for group in groups:
                        group.assign(step)

            groups = by_title.get(step.title, empty)
            for group in groups:
                group.assign(step)


class ParameterTable(dict[str, RowDict]):
    """
    A CSV table whose schema is as follows:
        - The first row are headers where each column specifies a header title
        - Each subsequent row is a dictionary of {header:cell} values added to this parameter table
        - Each row is keyed by its first column-cell when added to this table-dictionary

    e.g. this table has two entries keyed as "SetupA" and "SetupB"
        ------------------------------------------
        |        | ipv4        | timeout | retry |
        ------------------------------------------
        | SetupA | 172.17.3.4  | 5.5     | True  |
        ------------------------------------------
        | SetupB | 192.168.3.4 | 10      | False |
        ------------------------------------------
    """
    def __init__(self, cfg):
        dict.__init__(self)
        self.file: 'ConfigFile' = cfg
        self.keys = []
        self.name = ""
        self._ser = None

    def serialize(self) -> dict[str, Any]:
        if self._ser is None:
            self._ser = {"type": "ParameterTable", "keys": self.keys, "data": self, "name":self.name}
        return self._ser

    def _load(self, reader, priority):
        line, keys = _next_row(self.file, reader)
        self.keys = keys
        if keys is False: return priority
        while True:
            line, row = _next_row(self.file, reader)
            if row is False: return priority
            self[row[0]] = {k: text_eval(v) for k, v in zip(keys, row)}  # if v != ''
        return priority

    def find(self, where: dict[str, Any]) -> list[Any]:
        for k, row in self.items():
            if all(row[key] == val for key, val in where.items()):
                return row


class VParameterTable(dict[str, RowDict]):
    """
    This is identical to a ParameterTable except that the CSV representation is transposed
    This representation can be more ergonomic for certain kinds of configuration data sets.

    A CSV table whose schema is as follows:
        - The first column are headers where each row specifies a header title
        - Each subsequent column is a dictionary of {header:cell} values added to this parameter table
        - Each column is keyed by its first row-cell when added to this table-dictionary

    e.g. this table has two entries keyed as "SetupA" and "SetupB"
        --------------------------------------
        |         | SetupA     | SetupB      |
        --------------------------------------
        | ipv4    | 172.17.3.4 | 192.168.3.4 |
        --------------------------------------
        | timeout | 5.5        | 10          |
        --------------------------------------
        | retry   | True       | False       |
        --------------------------------------

    """
    def __init__(self, cfg):
        dict.__init__(self)
        self.file: 'ConfigFile' = cfg
        self.keys = []
        self.name = ""
        self._ser = None

    def serialize(self) -> dict[str, Any]:
        if self._ser is None:
            self._ser = {"type": "VParameterTable", "keys": self.keys, "data": self, "name":self.name}
        return self._ser

    def find(self, where: dict[str, Any]) -> list[Any]:
        for row in self:
            if all(row[key] == val for key, val in where.items()):
                return row

    def _load(self, reader, priority):
        line, row = _next_row(self.file, reader)
        if row is False: return priority

        # Parse first row and initialize one dict per entry name
        entrykey = row[0]
        self.keys = [entrykey]
        entrynames = row[1:]
        for entryname in entrynames:
            self[entryname] = {entrykey: entryname}

        while True:
            line, row = _next_row(self.file, reader)
            if row is False: return priority
            rowlen = len(row)
            k = row[0]
            self.keys.append(k)
            for i, entryname in enumerate(entrynames):
                if i+1 >= rowlen: continue
                v = row[i+1]
                # if v != '':
                self[entryname][k] = text_eval(v)
        return priority


Table: TypeAlias = ParameterTable | StepTable | GenericTable


class ConfigFile(dict[str, Table]):
    """
    Dictionary representation of a single config.csv file 
    containing any number of Table types keyed by their titles.
    """
    def __init__(self, path: str):
        self.relative_path = str(Path(path).relative_to(os.getcwd()))
        self.path = path
        self.rows: list[list[str]] = []  # csv file rows
        mtime = os.path.getmtime(path)
        self.mtime = mtime
        self._load_csv(path, mtime)

    def __repr__(self):
        return f"ConfigFile[{self.path}]"

    cache: dict[str, "ConfigFile"] = {}
    
    @classmethod
    def get_create(cls, path: str):
        cfgf = cls.cache.get(path)
        if not cfgf:
            cfgf = ConfigFile(path)
            cls.cache[path] = cfgf
        return cfgf

    def should_reload(self) -> float:
        """ 
        Returns truthy if the config has been modified since reading and should be reloaded from disk.
        @return:
            - 0 if config file isn't loaded or hasn't been modified since reading
            - modified time if file has been modified since reading
        """
        mtime2 = os.path.getmtime(self.path)
        if mtime2 <= self.mtime:
            return 0

        return mtime2

    def reload(self) -> bool:
        """
        @return: True if reload occured, False if not
        """
        mtime2 = os.path.getmtime(self.path)
        if mtime2 <= self.mtime:
            return False

        # Reload tables
        self._load_csv(self.path, mtime2)
        return True


    _type_map = {
        "parametertable": ParameterTable,
        "vparametertable": VParameterTable,
        "generictable": GenericTable,
        "steptable": StepTable
    }

    def _dump_csv(self, filepath: str):
        with open(filepath, mode='w', encoding="UTF8") as f:
            writer = csv.writer(f)
            writer.writerows(self.rows)

    def _load_csv(self, filepath: str, mtime: float):
        with open(filepath, mode="r", encoding="UTF8") as f:
            self.clear()
            self.rows.clear()
            self.mtime = mtime
            self._steptables_cfgstr = -1
            self._steptables_names = []
            self._steptables = None
            _group_priority = 0

            reader = csv.reader(f)
            try:
                while True:
                    line = reader.line_num
                    row = next(reader)
                    self.rows.append(row)

                    # Ignore empty row
                    if not len(row) or all(x.strip() == '' for x in row):
                        continue

                    # Ignore comment row
                    firstval = row[0]
                    if type(firstval) == str:
                        if firstval[0] == '\ufeff':
                            firstval = firstval[1:]
                            row[0] = firstval
                        if firstval.lstrip().startswith("#"):
                            continue

                    # Read table title and type
                    if len(row) < 2 or type(firstval) != str or type(row[1]) != str:
                        raise Exception("Csv Config: new table must begin with row denoting: [title, tabletype])")
                    title = firstval
                    tabletype = row[1].strip().lower()

                    try:
                        table: Table = self._type_map[tabletype](self)
                        table.name = title
                        self[title] = table
                        _group_priority += table._load(reader, _group_priority)
                    except KeyError:
                        raise Exception("Csv Config: table type must be in {}".format(list(self._type_map)))
            except StopIteration:
                pass


class Undefined:
    pass

class ConfigTables(dict[str, Table]):
    """
    Collection of Tables, indexed by their titles, sourced
    from a combination one or more files of type ConfigFile
    """
    def __init__(self, *files: ConfigFile | str | Path, warn_duplicates: bool = False):
        self.files: dict[str, ConfigFile] = {}
        self.warn_duplicates = warn_duplicates
        self._prev_globalsettings = ""
        self._prev_stepsettings = ""
        self._steptables_names: list[str] = []
        self._steptables: StepTable = None
        self._structure: dict[str, Any] = {}
        self.add_files(*files)

    def add_files(self, *files: ConfigFile | str | Path) -> "ConfigTables":
        """
        Include additional config.csv type files into this collection.
        
        NOTE: If the file(s) were already part of this collection then they are moved
        to the end, which in turn causes their tables to overwrite tables of
        the same name if duplicates exist.
        """
        for f in files:
            cfs = (f,) if type(f) == ConfigFile else (ConfigFile.get_create(x) for x in glob.glob(str(f)))
            for cf in cfs:
                self.files.pop(cf.path, None)
                self.files[cf.path] = cf
            
        self._rebuild()
        return self

    def get_parametertable(self, title: str, default=None) -> ParameterTable:
        """
        Gets table of type ParameterTable
        NOTE: this works well for both ParameterTable and VParameterTable as their software APIs are identical
        @param title: title of the table
        """
        return self.get(title, default)

    def get_generictable(self, title: str, default=None) -> GenericTable:
        """
        Gets table of type GenericTable
        @param title: title of the table
        """
        return self.get(title, default)

    def get_steptable(self, title: str, default=None) -> StepTable:
        """
        Gets table of type StepTable
        @param title: title of the table
        """
        return self.get(title, default)
    
    
    # def _make_steptable(self,):

        # # clear active
        # self.clear()

        # # reload cmdline args if they should be overwritten
        # if not cmdlineargs_overwrite_globalsettings:
        #     self.update(argsC)

        # # reload global settings
        # self._prev_globalsettings = globalsettings
        # global_settings_table = self.tables.get("GlobalSettings")
        # if global_settings_table:
        #     global_settings_row = global_settings_table.get(globalsettings)
        #     # retval["global"] = tuple((k, global_settings_row[k]) for k in global_settings_table.keys)
        #     if global_settings_row:
        #         self.update(global_settings_row)

        # # reload cmdline args if they should overwrite global values
        # if cmdlineargs_overwrite_globalsettings:
        #     self.update(argsC)

    def should_reload(self) -> bool:
        return any(x.should_reload() for x in self.files.values())

    def reload(self) -> bool:
        """
        Reload .csv file data from disk.
        NOTE: table data is only reloaded if config CSV has been updated since the last load
        """
        if any(x.reload() for x in self.files.values()):
            self._rebuild()
            return True
        return False

    def hydrate(self, target: 'ConfigStruct', sourcetables: Iterable[str] = ('GlobalSettings.Default',)):
        """
        @param target: config object to be filled with sourcetables
        @param sourcetables: A set of table-row specifiers: 'TableName.RowTitle', where each row
                             specified is injected into target one after the other.
        """
        if not sourcetables:
            sourcetables = ('GlobalSettings.Default',)
        
        target.clear()
        target._tables = self
        target._sourcetables = sourcetables

        if not target._cmdlineargs_overwrite_settings:
            target.update(argsC)

        for tabrow in sourcetables:
            tab_row = tabrow.split(".", maxsplit=1)
            tabname, rowname = tab_row if len(tab_row) >= 2 else (tab_row[0], "Default")
            table = self.get(tabname)
            if table is None:
                if tabname == "Settings":
                    table = self.get("GlobalSettings")  # legacy support
                    if table is None:
                        raise KeyError(f"Table: '{tabname}' does not exist in the config files: {', '.join(self._filenames)}, you may have mispelled one of your sourcetables entries.")
            row = table.get(rowname)
            if row is None:
                if rowname == "Default":
                    row = table.get("DefaultGlobalSettings")  # legacy support
                    if row is None:
                        raise KeyError(f"Row: '{rowname}' does not exist table '{table.name}' from '{table.file.relative_path}', you may have mispelled one of your sourcetables entries.")
            
            target.update(row)

        if target._cmdlineargs_overwrite_settings:
            target.update(argsC)

        for name, param in target._params.items():
            val = dict.get(target, name, Undefined)
            if val is Undefined:
                if param.required:
                    raise KeyError(f"'{type(target).__name__}' class requires parameter: '{name}' to exist in {', '.join(sourcetables)} from {', '.join(self._filenames)}")
                val = param.default
            setattr(target, name, val)
            
    def _rebuild(self):
        self.clear()

        for cfgf in self.files.values():
            if self.warn_duplicates:
                conflicts = set(cfgf).intersection(self)
                if conflicts:
                    print(f"\nWarning: {cfgf.path} overwriting duplicate tables from {self.path}: {conflicts}")
            self.update(cfgf)

        self._structure = {
            "tables": {k: v.serialize() for k, v in self.items()},
            "files": {x.relative_path: x.mtime for x in self.files.values()},
        }

    @property
    def _filenames(self) -> Iterator[str]:
        return (x.relative_path for x in self.files.values())


    def _reload(self, globalsettings="DefaultGlobalSettings", stepsettings="DefaultStepSettings",
               cmdlineargs_overwrite_globalsettings=True, reload_if_file_modified=True
                ) -> tuple[dict, StepTable]:
        """
        reload test_config.csv, cmdline args, and globalsettings data
        NOTE: table data is only reloaded if config CSV has been updated since the last load
        
        @param globalsettings: 
        @param cmdlineargs_overwrite_globalsettings: 
        """
        reloaded = False
        if any(x.reload() for x in self.files.values()):
            reloaded = True

        if reloaded or globalsettings != self._prev_globalsettings:
            self._refill(globalsettings, cmdlineargs_overwrite_globalsettings)
                
        if reloaded or stepsettings != self._prev_stepsettings:
            self._steptables_names = []
            self._prev_stepsettings = stepsettings

            steptables: list[StepTable] = []
            if stepsettings is None:
                pass
            elif type(stepsettings) == str:
                st = self.get_steptable(stepsettings)
                if st:
                    steptables.append(st)
            else:
                for steptablename in stepsettings:
                    st = self.get_steptable(steptablename)
                    if st:
                        steptables.append(st)

            self._steptables_names = list(x.name for x in steptables)
            self._steptables = StepTable._combine(self, steptables)

        retval = {"files": tuple(dict(path=x.path, mtime=x.mtime) for x in self.files.values()),
                  "args": argsC.serialize(),
                  "fieldargs": argsF.serialize(),
                  "tables": {k: v.serialize() for k, v in self.items()},
                  }


        return retval, self._steptables


class ConfigStruct(RowDict):
    """
    Base class whose subclasses can define typed config objects 
    to be loaded using ConfigTables.hydrate(...)

    NOTE: It is fine to shadow any property or field that doesn't
           start with an underscore in a derived class. 
    """

    _first_config: 'ConfigStruct' = None
    _params: dict[str, Parameter.Type] = None  # cls variable placeholder (shadowed by derived types)

    @property
    def tables(self) -> ConfigTables | None:
        return self._tables

    @property
    def cmdlineargs_overwrite_settings(self) -> bool:
        return self._cmdlineargs_overwrite_settings
    
    @cmdlineargs_overwrite_settings.setter
    def cmdlineargs_overwrite_settings(self, val: bool):
        self._cmdlineargs_overwrite_settings = val

    def __init__(self, cmdlineargs_overwrite_settings=True):
        self._tables: ConfigTables = None
        self._sourcetables: list[str] = []  # table cascade loaded into this TestConfig
        self._cmdlineargs_overwrite_settings = cmdlineargs_overwrite_settings
        self._reflect()

    def validate(self):
        """Validate a ConfigStruct as a whole
 
        When overridden, this method should do one of two things:
        - coerce values to within limits
        - throw a ValueError if some value violates its limits
        
        @throws: ValueError() if value(s) violate limits
        """
        pass

    def _reflect(self):
        cls = self.__class__
        p = getattr(cls, "_params", None)
        if p is None:
            p = cls._params = {
                name: param._init(name) 
                for name, param in cls.__dict__.items() 
                if isinstance(param, Parameter.Type)
            }
        
        for name, param in p.items():
            self[name] = param.default
            setattr(self, name, param.default)

    def _reload(self):
        tables = self._tables

        return {
            "files": tuple(dict(path=x.path, mtime=x.mtime) for x in tables.files.values()),
            "args": argsC.serialize(),
            "fieldargs": argsF.serialize(),
            "tables": {k: v.serialize() for k, v in tables.items()},
        }

    def _get_structure(self):
        return {name: param._to_dict(getattr(self, name)) for name, param in self._params.items()}


class TestState:
    def __init__(self, path: str, default: dict[str, Any] | None=None):
        """Initialize a new Test State

        @param path: file path to test_state.json
        @param default: data to use in the event that the path does not exist
        """
        self.path = path
        self.default = default
        self.state: dict[str, Any] = {}
        self.reload()

    def reload(self):
        try:
            with open(self.path, mode="r") as f:
                self.state = json.load(f)

            if self.default:
                for k, v in self.default.items():
                    if k not in self.state:
                        self.state[k] = v
        except:
            if self.default is not None:
                self.state = {}
                self.state.update(self.default)
            else:
                self.state = {}

    def save(self):
        with open(self.path, mode="w") as f:
            json.dump(self.state, f, indent=2, sort_keys=True)

    # Dictionary Passthrough Methods

    def get(self, key: str, default=None):
        return self.state.get(key, default)

    def __getitem__(self, key: str):
        return self.state[key]

    def __setitem__(self, key: str, val: Any):
        self.state[key] = val
        self.save()

    def __delitem__(self, key: str):
        del self.state[key]
        self.save()

    def __iter__(self):
        return self.state.__iter__()

    def items(self):
        return self.state.items()

    def update(self, adict: dict[str, Any] | None=None, **properties):
        if adict:
            self.state.update(adict)
        if properties:
            self.state.update(properties)
        self.save()


class TestDirectory(type(Path())):
    def insert_path(self, relative_path: str | Path = "."):
        """
        @param relative_path: testroot-relative directories to prepend to the sys.path 
            This can help an executable src/scratchpad.py file to use consistent import 
            statements to reference local python files/modules.
        """
        p = str(self.joinpath(relative_path).resolve())
        sys.path.insert(0, p)
    
    def __add__(self, other: str | Path) -> Path:
        return self.joinpath(other)


def locate_testroot(anchorfile: str | Path="config*.csv") -> TestDirectory:
    """
    Search the cwd (current working directory) for the anchorfile then bubble up the file system
    recursively until anchorfile is found.

    @param anchorfile: anchor file (filename or relative path) to search for
                       NOTE: this can be a glob pattern e.g. config*.csv

    @return: anchorfile directory Path
    """
    anchorfile = Path(anchorfile)

    if anchorfile.is_absolute():
        if not anchorfile.is_dir():
            anchorfile = anchorfile.parent
        return TestDirectory(anchorfile)

    cache_key = str(anchorfile)
    entry = _locate_cache.get(cache_key)
    
    if entry is None:
        # locate the config file by bubbling up the tree
        cdir = Path.cwd().resolve()
        while True:
            file = str(Path(cdir, anchorfile))
            options = glob.glob(file)
            if len(options) > 0:
                break
            if len(cdir.parts) == 1:
                raise FileNotFoundError(f"file '{anchorfile}' not found in any parent directories.")
            cdir = cdir.parent

        # record the cache
        entry = TestDirectory(cdir)
        _locate_cache[cache_key] = entry

    return entry


def get_testconfig(filepath: str | Path = "config*.csv", 
                   schema: Type[T] = ConfigStruct,
                   sourcetables: Iterable[str] = ('Settings.Default',),
                   cmdlineargs_overwrite_settings=True
                   ) -> T:
    """
    Create a ConfigStruct by searching the cwd and parent directories for config file(s) specified 
    in *filepath*, loading the files into a ConfigTables object, instantiating a config struct 
    of type *schema*, then hydrating the config struct using the table-rows specified in *sourcetables* 

    @param filename: file(s) to search for and load into ConfigTables
                     NOTE: this can be a glob pattern e.g. config_*.csv
    @param schema: ConfigStruct derivative to be instantiated, hydrated, and returned    
    @param sourcetables: A set of table-row specifiers: 'TableName.RowTitle', where each row
                         specified is injected into target one after the other.
    @param cmdlineargs_overwrite_settings: if True, any -Ckey=val commandline arguments will overwrite entries
                                           in this structure. If False, they will be overwritten by parameter
                                           defaults and table-row values.
                                                
    """
    cache_key = f"{filepath}:{schema}"
    entry = _testconfig_cache.get(cache_key)
    
    if entry is None:
        entry = schema(cmdlineargs_overwrite_settings)
        
        testroot = locate_testroot(filepath)
        tables = ConfigTables(testroot + filepath)
        tables.hydrate(entry, sourcetables)

        _testconfig_cache[cache_key] = entry
        if ConfigStruct._first_config is None:
            ConfigStruct._first_config = entry
        
        # entry.validate(), TODO consider this

    return entry


def get_teststate(file="state.json", default=EMPTY_DICT) -> TestState:
    """
    Creates a State object by searching the cwd for the file
    and bubbling up the directory structure recursively until finding its root location.
    """
    cache_key = str(Path(file).resolve())
    if cache_key in _teststate_cache:
        return _teststate_cache[cache_key]
    state = TestState(locate_testroot("config*.csv") + file, default=default or {})

    _teststate_cache[cache_key] = state
    return state


def get_testargs() -> dict[str, Any]:
    """
    Get dictionary of command line arguments passed to the process of form:
        -Ckey=value
    """
    args = {}
    args.update(argsC)
    return args


def _next_row(fcfg: ConfigFile, reader) -> tuple[int, list[str]]:
    while True:
        line = reader.line_num
        row = next(reader)
        fcfg.rows.append(row)
        # empty row signifies end of table
        if not len(row) or all(x.strip() == '' for x in row):
            return -1, False
        # ignore comment rows
        if type(row[0]) == str and row[0].lstrip().startswith("#"):
            continue
        return line, row


# def ensure_template_instance(*files: str | Path):
#     for f in files:
#         inst = Path(f, "station.py")
#         station = Path(testroot, "station.py")
#         if station.exists():
#             return
#         template = Path(testroot, "station.template.py")
#         if template.exists():
#             with open(str(template)) as f:
#                 template_txt = f.read()
#             with open(str(station), mode="w") as f:
#                 f.write(template_txt)
#                 f.flush()
