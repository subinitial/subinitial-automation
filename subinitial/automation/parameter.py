from typing import Any
from urllib import request
from .utils import text_eval


AUTO = None
UNDEFINED = None


class Parameter:
    """Collection of Parameter types useful in specifying fields from config.csv style spreadsheet tables"""

    class Type:
        _eval_type = None

        def __init__(self, default=None, units=UNDEFINED, required=True, **kwargs):
            self.name = None
            self.default = default
            self.required = required
            self.units: str = units
            self.kwargs = kwargs

        @property
        def type(self) -> str:
            return self.__class__.__name__

        def validate(self, val) -> bool:
            """Check a value validity for this Type.
             
             This method should do one of three things:
                - return the value as is to ignore any limitations
                - coerce the value to within limits and return the coerced value
                - throw a ValueError if the value violates its limits
            
            @param val: value based on this type
            @return: value either as-is, or modified
            @throws: ValueError() if value violates its limits
            """
            return val

        def tostr(self, val: Any) -> str:
            """converts a value to string based on this type
            
            @param val: value to convert to string
            @return: string representation of val
            """
            if val is None:
                return ""
            return str(val)

        def parse(self, str_repr: str) -> Any:
            """converts a string representaion to value based on this type
            
            @param str_repr: string representation of value
            @return: value of parsed str_repr
            """
            return text_eval(str_repr, self._eval_type)

        def _init(self, name):
            self.name = name
            return self

        def _to_dict(self, value):
            return dict(
                value=self.tostr(value),
                units=self.units,
            )

    class Int(Type, int):
        """Integer type parameter"""
        _eval_type = int

    class Float(Type, float):
        """Float type parameter"""
        _eval_type = float

    class String(Type, str):
        """String type parameter"""
        _eval_type = str

    class ListType(Type):
        def __init__(self, default=AUTO, title=AUTO, units=UNDEFINED, **kwargs):
            if default is AUTO:
                default = list()
            Parameter.Type.__init__(self, default, title, units, **kwargs)

        def parse(self, str_repr: str) -> list:
            val = text_eval(str_repr)
            return val if type(val) == list else list(val)

    class IntList(ListType, list[int]):
        pass

    class FloatList(ListType, list[float]):
        pass

    class StringList(ListType, list[str]):
        pass

    class DictType(Type):
        def __init__(self, default=AUTO, title=AUTO, units=UNDEFINED, **kwargs):
            if default is AUTO:
                default = dict()
            Parameter.Type.__init__(self, default, title, units, **kwargs)

    class IntDict(DictType, dict[str, int]):
        pass

    class FloatDict(DictType, dict[str, float]):
        pass

    class StringDict(DictType, dict[str, str]):
        pass
