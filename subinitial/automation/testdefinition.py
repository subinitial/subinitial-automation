# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework Test Definition and structure
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import traceback
import sys
import time
import os
from typing import Iterator, Any, Callable, Type, Iterable

from .assertions import AssertionBase
from .field import FieldCollection
from .step import Step, StepCollection, _freeze_instance_data
from .testconfig import get_testconfig, StepTable, ConfigStruct
from .interactive import *
from .reporters import ConsoleReporter, InteractiveReporter, AggregateReporter
from .utils import ConnectionManager, TriageRefs, argsC, argsF, Task


class Report:
    def __init__(self, test):
        self.title = "TestReport"
        self.failcount = 0
        self.passcount = 0
        self.assertion_failcount = 0
        self.passed: list[Step] = []
        self.failed: list[Step]  = []
        self.skipped: list[Step]  = []
        self.test: TestDefinition = test

    def reset(self):
        self.failcount = 0
        self.passcount = 0
        self.skipcount = 0
        self.assertion_failcount = 0
        self.assertion_evalcount = 0
        self.passed = []
        self.failed = []
        self.skipped = []

    def _tallynode(self, node: Step):
        if node.result is None:
            self.skipped.append(node)
            return
        elif node.result is True:
            self.passed.append(node)
        elif node.result is False:
            self.failed.append(node)
        if (node.result is True or node.result is False) and node.assertions is not None:
            for assr in node.assertions:
                self.assertion_evalcount += 1
                if assr.result is False:
                    self.assertion_failcount += 1
        for child in node.steps:
            self._tallynode(child)

    def generate_summary(self):
        self.reset()
        for node in self.test.steps:
            self._tallynode(node)
        self.passcount = len(self.passed)
        self.failcount = len(self.failed)
        self.skipcount = len(self.skipped)


class Action:
    def __init__(self, title, function=None, subtitle=None, is_async=True, classlist=None, operator_mode=False):
        self.idx = -1
        self.function: Callable = function
        self._ctx: threading.Thread = None
        self.title = title
        self.subtitle = subtitle
        self.is_async = is_async
        self.operator_mode = False
        self.classlist = classlist if classlist else []
        self.retval = None

    @staticmethod
    def _exe_function(action: "Action", atp: "TestDefinition"):
        action.retval = action.function(action, atp)
        atp.reporter.report_action(action)

    def _get_structure(self):
        return {"idx": self.idx, "title": self.title, "subtitle": self.subtitle, "is_async": self.is_async,
                "classlist": self.classlist, "operator_mode": self.operator_mode, "retval": self.retval}

    def execute(self, atp: "TestDefinition" = None):
        ret = None
        if self.function:
            if self.is_async:
                if self._ctx and self._ctx.is_alive():
                    ret = "@busy"
                else:
                    t = self._ctx = threading.Thread(target=self._exe_function, args=(self, atp))
                    t.daemon = True
                    t.start()
                    ret = "@started"
            else:
                self._exe_function(self, atp)
                ret = self.retval
        return ret


class TestDefinition:
    """Toplevel Test Definition

    The test developer subclasses TestDefinition to define a toplevel test that can perform an automated test procedure.
    A subclassed TestDefinition or test (shorthand) should define a test tree sequence, fields queried from the operator,
    equipment connection / disconnection logic, pre-run and post-run logic.
    """

    @staticmethod
    def get_datetime(fmt="%y-%m-%d_%H-%M-%S"):
        return Step.get_datetime(fmt)

    class Data:
        pass

    def on_step_failure(self, step: Step):
        """Called each time a Step result is False (FAIL)
        
        :param step: Step that is reporting a failure
        """
        pass

    def init(self):
        """Called once after instance is constructed
            -programatic definition of static data
            -Called once as suite.start() is entered
        """
        pass

    def define_test(self, data: Data):
        """Build a test tree & define test fields.
            TestCenter calls this right away when loading a Suite to generate a preview of the full Test Procedure Tree
            TRy not to require hardware calls or network connections here
        """
        pass

    def on_start(self, data: Data):
        pass

    @property
    def step_triage_msg(self):
        """ Triage text message that appears before any triage reference designators
        :rtype: str
        """
        return getattr(self.data, "step_triage_msg", "Inspect/replace the following parts:")

    def get_triage_steps(self):
        """Gets a list of failed steps with valid triage values
        :return: list of failed steps with valid triage values
        :rtype: list[Step, str, TriageRefs]
        """
        if self._triage_steps:
            return self._triage_steps
        for step in self.walk():
            result, msg, refs = step.result, step.step_triage_msg, step.step_triage_refs
            if result is False and (msg or refs):
                if msg is None:
                    msg = self.step_triage_msg
                if refs is not None:
                    refs = TriageRefs(refs)
                self._triage_steps.append([step, msg, refs])
        return self._triage_steps

    def get_triage_json(self):
        """Generates a parsable JSON set of triage information"""
        triage_list = []
        for step, msg, refs in self.get_triage_steps():
            x = [step.title, msg]
            if refs:
                x += refs.tolist()
            triage_list.append(x)
        return json.dumps(triage_list)

    def add_action(self, function, title="", subtitle=None, threadsafe=True, classlist=None):
        if isinstance(function, Action):
            act = function
        else:
            act = Action(title, function, subtitle, threadsafe, classlist)
        act.idx = len(self.actions)
        self.actions.append(act)
        return function

    def _connect(self) -> tuple[str, str]:
        reporter = self.reporter
        if not self.connected:
            reporter.report_connecting()
            try:
                ret = self.connect(self.data)
                if ret:
                    self.reporter.report_disconnected()
                    if type(ret) == list:
                        return ConnectionManager.stringify_errors("Connect", ret), ""
                    else:
                        return str(ret), ""
                    
            except Exception as e:
                trace = traceback.format_exc()
                self.reporter.report_disconnected()
                return str(e), trace

            self.connected = True
            self.reporter.report_connected()
        
        return "", ""

    def _disconnect(self) -> tuple[str, str]:
        reporter = self.reporter
        if self.connected:
            reporter.report_disconnecting()
            try:
                ret = self.disconnect(self.data)
                if ret:
                    self.reporter.report_connected()
                    if type(ret) == list:
                        return self.reporter.report_error(ConnectionManager.stringify_errors("Disconnect", ret)), ""
                    else:
                        return str(ret), ""
            except Exception as e:
                trace = traceback.format_exc()
                self.reporter.report_connected()
                return str(e), trace

            self.connected = False
            reporter.report_disconnected()
        return "", ""

    def connect(self, data: Data):
        """Connect to the test equipment and test fixture.

            This method is a good place to verify that the propert equipment and fixture exist and are operating correctly.

        :return: a value indicating any errors that occured when trying to connect
            None: no errors occured
            list: a list of error messages or values that occured during the connect method, any items in this list that
                are None will be filtered, and an empty list will be treated as if no errors occured.
                e.g. you can use the return value for TaskList.wait_for_tasks() here.
            other values: error message or value that occured during the connect method
        """
        pass

    def disconnect(self, data: Data):
        """Disconnect from test equipment and test fixture.
        
            This is only called if a previously connect() call completed successfully without errors."""
        pass

    # def before_requiring_all_field_to_be_final_after_clicking_start(self):
    #     """Construct some fields definitions. The test will run as soon as the user provides data
    #         possibly connect to hardware & perform auto-id
    #     """
    #     pass

    def pre_run(self, data: Data):
        """User clicked start...we're about to run the test, connect to hardware if you need to
            User supplied field entries are available and can be used to modify the procedure before running
            -a good place to compile a test config dictionary based on
                -user provided field values
                -auto id table lookups
                -config.csv tables
            -after pre_run returns, the final field values will get passed to the reporter
        """
        # auto id
        # populate field values
        # if any field values are insufficient, loop asking for reporter.query_dict() until good
        # compile test config dictionary
        pass

    def post_run(self, data: Data):
        """Called after test procedure has completed
            -a good place to update and save test State
            -a good place to save reports
            -a good place to put in auto insertion
        """
        # update states
        # publisher.save()
        # while True:
        #     wait_for_auto_insertion()
        #     if check_for_cancel():
        #         break
        # return self.reporter.query_user("Continue? (y or n):") == "y"
        pass

    def on_exit(self, data: Data):
        """ Called once before exiting the test suite.
                -the call to suite.start() returns after this method is finished
        """
        pass

    def __init__(self, title="Automated Test", debug=False, target=None, skips=None, final=None, **properties):        
        self._properties = properties
        self.skip_field_prompt = False
        """if True, the reporter will be discouraged from prompting the user for Fields
            and instead be encouraged to start the test procedure automatically. This
            can enable the test writer to design automatic field lookups or custom
            user prompts before and after the test procedure runs"""
        
        self.run_once = False
        """if True, exit the start() method after one test run has completed"""

        self.connection_helptext = "Connection Error:\n" \
            "Please verify proper connections and power to the " \
            "test fixture, DUT, test equipment, and network if applicable then try again."

        self.station = {}

        self._thread_step = None
        self._reload_queued = False
        """set by users in UI/TestCenter"""
        self.breakpoints: set[int] = set()
        self.skippoints: set[int] = set()
        self._target_query = target
        self._skips_query = skips
        self._final_query = final
        self._subset = None
        self.target: Step|None = None
        self._target_searching = False
        self.final: Step = None
        self._final_reached = False
        self.guictrl = None

        self._triage_steps: list[list[Step, str, TriageRefs]] = []
        self.skip_triage_prompt = False

        self.title: str = title
        self.start_time: float = None
        self.run_time: float = None
        self.fields = FieldCollection()
        self.steps = StepCollection(self)
        self._datalist = [None]
        self.persistent_steps: list[Step] = []

        self.actions: list[Action] = []

        self.parent = None  # test definitions have no parents =(

        self.requirement_steps: list[Step] = None

        for arg in sys.argv:
            if arg in ('--si-debug', '-d'):
                debug = True

        self.debug = debug

        self._configdata = {}
        self.config: ConfigStruct = None
        self._steptables: StepTable = None
        self.stepsettings_sourcetables: Iterable[str] = ('StepSettings',)

        self.step_tags = []

        self.groups = []

        self.print_traceback = True
        self._abort = False
        self.state = "stopped"  # 'paused', 'running'

        self.tags = []
        self.data = object()

        self.version = "v0.1.0"
        self.doc_teststeps = "SD00xxx"
        self.doc_testplan = "SD00xxx"

        self.connected = False

        self.start_conditions: list[dict[str:str]] = []

        self.disconnect_after_run = True

        # If True then all test steps are run even if one fails. If false, stop on first failure
        self.is_resilient = False

        # If True then all the test will stop on the first failed assertion
        self.stop_on_assertion_fail = False

        # The final result of the test after run() is finished
        self.result = None
        """:type: bool"""

        self.step_retry: int | str | None = None
        """If steps of this test-definition should retry upon failure,
                    -if a number then it will limit the number of retries
                    -if "prompt" then the test operator will be prompted to retry (yes/no)
                    -(default) if 0 or None then the test will not retry upon failure
                """

        self._report = None
        self._depth = 0
        self._disonnection_failed_after_run = None

        self.reporter = ConsoleReporter(self)
        self.ireporter = None

        self._interactive_started = False
        self._remotecommander: RemoteCommander = None
        self._localcommander: StdioCommander = None

        self.init()


    def _close(self):
        self.state = "closing"
        self._abort = True
        return True

    def _stop(self):
        self.state = "stopping"
        self._abort = True
        return True

    def _wait_start(self, commander: Commander):
        while True:
            op = commander.ctrlqueue.get()
            if op == "close":
                return "close", self._close()
            if op == "connect":
                errmsg, trace = self._connect()
                if errmsg:
                    self.reporter.report_error(errmsg, trace)
                    self.reporter.report_info(self.connection_helptext)
            if op == "disconnect":
                errmsg, trace = self._disconnect()
                if errmsg:
                    self.reporter.report_error(errmsg, trace)
                    self.reporter.report_info(self.connection_helptext)
            if type(op) == dict:
                cmd = op.get('cmd')

                self.station = op.get("station", self.station)

                if cmd == "start":
                    arg = op.get('arg')
                    if arg == "pause":
                        self.state = 'paused'
                    else:
                        self.state = 'running'
                    if arg and type(arg) == dict:
                        self._target_query = arg.get("target")
                        self._skips_query = arg.get("skip")
                        self._final_query = arg.get("final")
                        self._subset = arg.get("title", None)
                    return "start", (op.get('fields'), op.get("station"), op.get('mtime'))

                if cmd == "reload":
                    self.testconfig_filename = op.get('config-file', self.testconfig_filename)
                    self.active_configuration = op.get('configuration', self.active_configuration)
                    return "reload", None

    def _wait_paused(self, iareporter: InteractiveReporter, commander: Commander):
        while True:
            iareporter.notify({"cmd": "state", "state": "paused"})
            op = commander.ctrlqueue.get()
            if op == "resume":
                self.state = "running"
                iareporter.notify({"cmd": "state", "state": "running"})
                return False
            if op == "step-into":
                self.state = "paused"
                return False
            if op == "stop":
                return self._stop()
            if op == "close":
                return self._close()

    def _should_reload_config(self):
        if self.config:
            return self.config._tables.should_reload()
        return False

    def _check_ctrl(self, iareporter: InteractiveReporter, commander: Commander, tid=None):
        """processes ctrl from a commander, returns True if stop is commanded"""
        if self.state == 'paused' or tid in self.breakpoints:
            return self._wait_paused(iareporter, commander)
        elif not commander.ctrlqueue.empty():
            try:
                op = commander.ctrlqueue.get_nowait()
            except Exception:
                return False
            if op == 'pause':
                self.state = 'paused'
                return self._wait_paused(iareporter, commander)
            if op == "stop":
                return self._stop()
            if op == "close":
                return self._close()
            # if op is None:
            return False

    def _initialize_data(self, **properties):
        """initializes the class data object by
            1. instantiating the class.Data
            2. copying static data into instance data
            3. calling the data instances setup method if one exists

        :rtype: TestDefinition.Data
        """
        # resolve the correct class type for data
        cdata = self.__class__.__dict__.get("Data", TestDefinition.Data)
        data = cdata()  # instantiate a fresh data
        _freeze_instance_data(data, cdata)

        # add injected properties passed to constructor
        key = None
        try:
            for key, val in properties.items():
                setattr(data, key, val)
        except AttributeError as ex:
            errmsg = "\nAttributeError: data in {} TestDefinition has no field: '{}'\n" \
                     "\tAdd '{}' to {}.Data\n\tor check spelling in your TestDefinition constructor as well"\
                .format(type(self).__name__, key, key, type(self).__name__)
            print(errmsg)
            sys.exit(1)
        return data

    @property
    def report(self) -> Report:
        if self._report is None:
            self._report = Report(self)
        return self._report

    @property
    def index(self):
        return 0

    def prompt_fields(self, *fields):
        """ DEPRECATED """
        r = self.reporter
        if r:
            return r.prompt_fields(*fields, step=self)
        return None

    def prompt_input(self, prompt: str, title: str | None=None, markdown=False) -> str | None:
        """ 
        Prompt the operator for some textual input 
        @param title: set modal window title in TestCenter, or print a title line in console.
        @param markdown: render prompt as markdown in TestCenter, (ignored in concole)
        @return: the operator's entry
        """
        r = self.reporter
        if r:
            return r.prompt_input(prompt, step=self, title=title, markdown=markdown)
        return None

    def prompt_continue(self, prompt: str, can_cancel=False, title: str | None=None, markdown=False, **kwargs) -> bool:
        """ 
        Prompt the operator before continuing forward with the test 
        @param can_cancel: allow the operator to select a negative response (or click out of the modal in TestCenter)
        @param title: set modal window title in TestCenter, or print a title line in console.
        @param markdown: render prompt as markdown in TestCenter, (ignored in concole)
        @return: True of continue, False if cancelled
        """
        can_cancel = can_cancel or kwargs.get("cancel", False)  # deprecated 'cancel' vs can_cancel nomenclature
        r = self.reporter
        if r:
            return r.prompt_continue(prompt, can_cancel=can_cancel, title=title, step=self, markdown=markdown)

    def report_info(self, *infotext: Any, clear=False):
        """ Report some textual information to display neatly with test output """
        r = self.reporter
        if r:
            r.report_info(" ".join(str(x) for x in infotext), step=self, clear=clear)

    def report_error(self, error: Exception | str, traceback=""):
        """ Report a caught exception and optional traceback to display neatly within test output """
        r = self.reporter
        if r:
            r.report_error(error, traceback=traceback, step=self)

    def _compile_test(self):
        """ index all steps and mock evaluate to build default assertions lists """
        for i, step in enumerate(self.walk()):
            step.toplevel = self
            step._index = i+1
            step._depth = step.parent._depth + 1
            step._assign_steptablegroups()
        
        for step in self.walk():
            step.post_init(step.data)
            step._mock_evaluate(self)

    def _reload_config(self):
        # reload csv tables
        did_reload_from_disk = self.config._tables.reload()
        
        # if needed, re-hydrate the test's config object (often this is the global variable: config)
        if did_reload_from_disk:
            self.config._tables.hydrate(self.config, self.config._sourcetables)
        
        cfgfile = None

        steptables: list[StepTable] = []
        for title in self.stepsettings_sourcetables:
            # try to find the StepTable
            st = self.config._tables.get_steptable(title)
            
            if st is None:
                if title == "StepSettings":
                    st = self.config._tables.get_steptable("DefaultStepSettings")
                if st is None: 
                    raise Exception(f"No StepTable named {title} found in csv config files!")
            
            # keep the last cfg file found
            cfgfile = st.file
            steptables.append(st)
        
        if cfgfile is None:
            cfgfile = next(self.config._tables.files.values())

        self._steptables = StepTable._combine(cfgfile, steptables)
        
        # create _configdata structure for communicating test config to TestCenter
        self._configdata = {
            "files": tuple(dict(path=x.path, mtime=x.mtime) for x in self.config.tables.files.values()),
            "settings": self.config._get_structure(),
            "args": argsC.serialize(),
            "fieldargs": argsF.serialize(),
            "stepsettings": tuple(x.serialize() for x in steptables)
        }
        self._reload_queued = False

    def _register_persistent_step(self, step):
        if step not in self.persistent_steps:
            self.persistent_steps.append(step)

    def _register_requirement_step(self, step):
        if self.requirement_steps is None:
            self.requirement_steps = [step]
        elif step not in self.requirement_steps:
            self.requirement_steps.append(step)

    def run(self, is_mock=False):
        """ run the full test procedure """
        self.report.reset()

        reporter = self.reporter
        reporter.report_run(self)

        self.start_time = time.time()

        self.result = True
        self._depth = 0
        for step in self.steps:
            chresult = step._run(self, is_mock)
            if self._abort:
                break
            if self._final_reached or self.final == step:
                self._final_reached = True
                mark_skip = False
                for s in self.walk():
                    if s == self.final:
                        mark_skip = True
                    elif mark_skip and s.result is None:
                        s._clear_assertions()
                        s._depth = 0
                        if self.final.has_ancestor(s) or self.final.is_sibling(s):
                        # reporter.report_stepstart(s)
                            reporter.report_criteria(s)
                            reporter.report_stepresult(s)
                break
            if chresult is False or chresult == "EXIT":
                self.result = False
                if step.step_is_requirement or (not self.is_resilient):
                    break
        self._depth = 0
        if not is_mock:
            # Teardown any persistent steps
            steps = list(reversed(self.persistent_steps))
            if self.requirement_steps:
                steps = list(reversed(self.requirement_steps)) + steps
                self.requirement_steps.clear()
            for step in steps:
                try:
                    step.teardown(step.data)
                except Exception as e:
                    reporter.report_info("in teardown() for {}:".format(step.title))
                    trace = traceback.format_exc() if self.print_traceback else None
                    reporter.report_error(e, trace)
            self.persistent_steps.clear()

        self.run_time = time.time() - self.start_time

        self.report.generate_summary()
        result = reporter.report_summary(self)

        if self.state == "stopping":
            self._abort = False
        return result

    def _reset_testdefinition(self):
        """ rebuild the entire test definition """
        self.steps.clear()
        self.persistent_steps.clear()
        self.fields.clear()
        self.run_time = None
        self.result = None
        self._abort = False
        self._final_reached = False
        self._disonnection_failed_after_run = None
        self._triage_steps = []
        Step._titleidx = dict()
        self._reload_config()
        self.data = self._initialize_data(**self._properties)
        self.define_test(self.data)
        self._steptables.assign(self)
        self._compile_test()
        

    def start(self, autorun=False) -> tuple[int, int]:
        """ 
        Start the test.

        @param autorun:
            if autorun is True or self.debug == True, then 
                self.skip_field_prompt = True
                self.run_once = True
        
        if the test is run in interactive mode (e.g. via TestCenter), then
            self.skip_field_prompt = False
            self.run_once = False

        @return: (pass_count, fail_count)
        """

        if autorun:
            self.skip_field_prompt = True
            self.run_once = True

        pass_fails = [0, 0]
        self.data = self._initialize_data(**self._properties)
        
        if self.config is None:
            self.config = ConfigStruct._first_config or get_testconfig()
        
        self._load_interactive()
        try:
            while True:
                self._reset_testdefinition()
                self.on_start(self.data)
                self._start_interactive()
                while True:
                    command = self.reporter.report_start(self)
                    self.target = self.find(self._target_query)
                    unskips = []
                    if self._skips_query is not None:
                        new_skippoints = set()
                        for q in self._skips_query:
                            t = self.find(q)
                            if t:
                                new_skippoints.add(t.tid)
                        unskips = list(self.skippoints - new_skippoints)
                        self.skippoints = new_skippoints
                    self.final = self.find(self._final_query)
                    if self.target:
                        self._target_query = self.target.tid
                    if self.final:
                        self._final_query = self.final.tid
                    if not self._subset and (self.target or self.final):
                        if self.target == self.final:
                            self._subset = "TID:{}({})".format(self.target.tid, self.target.title)
                        elif self.target and self.final:
                            self._subset = "TID:{}({}) - TID:{}({})".format(self.target.tid, self.target.title, self.final.tid, self.final.title)
                        elif self.target:
                            self._subset = "TID:{}({}) - end".format(self.target.tid, self.target.title)
                        else:
                            self._subset = "start - TID:{}({})".format(self.final.title, self.final.tid)
                    self.reporter.report_metadata({"target": self.target.tid if self.target else None,
                                                   "final": self.final.tid if self.final else None,
                                                   "skips": list(self.skippoints),
                                                   "unskips": unskips})
                    if command == "close":
                        break
                    elif command == "reload":
                        self._reload_queued = True
                        break

                    errmsg, trace = self._connect()
                    if errmsg:
                        self.reporter.report_error(errmsg, trace)
                        self.reporter.report_info(self.connection_helptext)
                        if self.run_once:
                            self.print_traceback = False
                            raise Exception("Failed to connect...")
                    else:
                        break
                if command == "reload":
                    continue
                elif command == "close":
                    break

                if self.pre_run(self.data) is False:
                    continue

                result = self.run()
                if result:
                    pass_fails[0] += 1
                else:
                    pass_fails[1] += 1

                post = self.post_run(self.data)
                if self.disconnect_after_run:
                    errmsg, trace = self._disconnect()
                    self._disonnection_failed_after_run = errmsg
                    if errmsg:
                        self.reporter.report_error(errmsg, trace)
                        
                        # since self.disconnect() has provided an error message / hint, we will not encumber 
                        # the user with additional Python tracebacks and instead assume the self.disconnect() 
                        # msg provides enough context for the user to resolve the issue e.g: bad LAN cable, 
                        # power switch off, etc.
                        self.print_traceback = False
                        raise Exception("Failed to disconnect after test run finished...")

                if self.result is False and not self.skip_triage_prompt and self.get_triage_steps():
                    joiner = "\n"
                    can_cancel = False
                    lines = []
                    if not self.ireporter:
                        for step, msg, refs in self.get_triage_steps():
                                lines.append("TRIAGE for {}:".format(step.title))
                                lines.append("\t{}".format(msg))
                                if refs:
                                    lines.append("\t\t{}".format(refs.tostr()))
                                lines.append("")
                    else:
                        joiner = ""
                        # can_cancel = True
                        for step, msg, refs in self.get_triage_steps():
                            lines.append(f'<div class="triage-step">')
                            lines.append(f'<h2><span>Triage for</span><span>TID:{step.tid}</span><span>{step.title}</span></h2>')
                            lines.append(f'<div class="triage-msg">{msg}</div>'.format(msg))
                            if refs:
                                lines.append(f'<div class="triage-refs"><span>References:</span>{refs.tostr()}</div>')
                            lines.append(f'</div>')
                    reporter = getattr(self.reporter, "reporters", None)
                    if reporter:
                        reporter = reporter[0]
                    else:
                        reporter = self.reporter
                    reporter.prompt_continue(joiner.join(lines), can_cancel=can_cancel, title="Test Failed", markdown=True)
                if post is False:
                    break
                elif self.run_once:
                    break
                if self._abort:
                    break
        except Exception as ex:
            trace = traceback.format_exc() if self.print_traceback else "   Exiting now..."
            self.reporter.report_error(ex, trace)
            self.print_traceback = True
            if self.debug:
                raise ex
        if self._disonnection_failed_after_run:
            self.reporter.report_info(self.connection_helptext)
        else:
            errmsg, trace = self._disconnect()
            if errmsg:
                self.reporter.report_error(errmsg, trace)
                self.reporter.report_info(self.connection_helptext)
        self.on_exit(self.data)
        self._stop_interactive()
        if os.environ.get("SI_REPEAT"):
            print(json.dumps(pass_fails))
        return pass_fails

    def find(self, query: str | int | Type | Callable) -> Step:
        """
        Get first step that matches the query
        @param query: query string "AStepTitle", ".ClassName", "@TagName", integer TID, ClassTypeObject, callable
        @param default: value to return if query not found
        @return: child Step if found, default if provided, else throw SubinitialTestException
        """
        if isinstance(query, Step):
            return query
        if query is None:
            return None
        for step in self.steps:
            selector = step._make_selector(query)
            if selector(step):
                res = step
            else:
                res = step.get_descendant(selector, None)
            if res:
                return res

    def find_all(self, query: str | int | Type | Callable) -> list[Step]:
        """Finds all steps that match the query
            :param query: query string "AStepTitle", ".ClassName", "@TagName", integer TID, ClassTypeObject, callable
            :return: list of Steps if found, else empty list
        """
        selector = Step._make_selector(query)
        found = []
        for step in self.walk():
            if selector(step):
                found.append(step)
        return found

    def _walk(self, param=None) -> Iterator[Step]:
        if param:
            yield param
        for s in self.walk():
            yield s

    def walk(self) -> Iterator[Step]:
        for s in self.steps:
            for p in s._walk():
                yield p

    def execute(self, action_idx: int) -> dict[str, Any]:
        action = None
        try:
            action = self.actions[action_idx]
            res = action.execute(self)
            retval = {"result": res, "error": None, "is_async": action.is_async}
        except Exception as ex:
            retval = {"result": None, "error": str(ex)}
        return retval

    def _get_structure(self, init=False) -> dict[str, Any]:
        cnodes = []
        
        try:
            gui = self.guictrl.model.serialize()
        except:
            gui = None
        
        node = {
            "title": self.title, "steps": cnodes,
            "actions": list(a._get_structure() for a in self.actions),
            "result": AssertionBase.get_resultset(init, self.result),
            "guictrl": gui,
            "start_conditions": self.start_conditions
        }

        node["fields"] = self.fields._get_structure()
        node["config"] = self._configdata

        for child in self.steps:
            cnode = child._get_structure(init)
            cnodes.append(cnode)
        return node

    def _get_interactive_args(self):
        """
        look up cmd line args and environment variables to determine the interactive arguments
        """
        address = os.environ.get("SI_ADDRESS", None)
        port = None
        machine_interface = (os.environ.get("SI_MI") == '1') or None
        interactive = (os.environ.get("SI_INTERACTIVE") == '1') or None
        autorun = (os.environ.get("SI_AUTORUN")) or os.environ.get("SI_REPEAT") or None

        for arg in sys.argv:
            if arg.startswith('--si-address='):
                address = arg[len('--si-address='):]
            elif arg == '--si-interactive':
                interactive = True
            elif arg == '--si-mi':
                machine_interface = True
            elif arg == "--si-autorun":
                autorun = True

        if interactive is not None and address is not None:
            # Parse Remote Args
            port = 11337
            if address:
                elems = address.split(":")
                if len(elems) >= 2:
                    address = elems[0]
                    port = int(elems[1])

        return interactive, machine_interface, address, port, autorun

    def _check_for_abort(self):
        if self._abort:
            return True
        commander = self._remotecommander or self._localcommander
        if commander:
            ctrlqueue = commander.ctrlqueue
            if not ctrlqueue.empty():
                try:
                    op = ctrlqueue.get_nowait()
                except Exception:
                    return False
                if op == 'pause':
                    self.state = 'paused'
                if op == "stop":
                    return self._stop()
                if op == "close":
                    return self._close()
        return False

    def _load_interactive(self):
        """
        based on cmdline params or environment variables the test definition can automaticlly
        reformat itself to run via an interactive commander and reporter such that it can be
        remotely operated by a separate process, program, debugger, or TCP client

        if the user has specified a custom reporter to the test it will still be used, but calls to its
        methods will be chained after the interactive reporter, and a flag will be provided to suggest
        that it should never block for user input (as the interactive reporter should be handling all
        user input).
        """
        is_interactive, use_machine_interface, server_address, server_port, autorun = self._get_interactive_args()

        if not is_interactive:
            if autorun or self.debug:
                self.skip_field_prompt = True
                self.run_once = True
            return False

        self.skip_field_prompt = False
        self.run_once = False

        # Create a local commander
        if use_machine_interface:
            self._localcommander = PipeCommander(self)
        else:
            self._localcommander = StdioCommander(self)

        # Create a remote commander if args permit and construct an interactive reporter
        if server_address is not None:
            self._remotecommander = RemoteCommander(self, server_address, server_port)
            StdioCommander.HIDE_OUTPUT = True
            self.ireporter = InteractiveReporter(self, self._remotecommander)
        else:
            self.ireporter = InteractiveReporter(self, self._localcommander)
        
        if self.guictrl:
            model = self.guictrl.model
            def notify():
                self.ireporter.report_guictrl(dict(model=model.serialize()))
            self.guictrl.model.on_notify = notify

        # # Add the interactive reporter to this test
        # if use_machine_interface or not self.reporter:
        #     self.reporter = self.ireporter
        # else:  # is_interactive AND not using machine_interface
        #     self.reporter = AggregateReporter(self, self.ireporter, self.reporter)
        self.reporter = AggregateReporter(self, self.ireporter, self.reporter)

        return True

    def _start_interactive(self):
        self._jstructure = self._get_structure(init=True)

        if not self._interactive_started:
            self._interactive_started = True
            if self._localcommander:
                self._localcommander.start()
            if self._remotecommander:
                self._remotecommander.start()

    def _stop_interactive(self):
        if self._localcommander:
            self._localcommander.stop()
        if self._remotecommander:
            self._remotecommander.stop()
