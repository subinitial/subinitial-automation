# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework Field classes
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from typing import Any, Iterator, Callable
from .utils import argsF


Validation = Callable[['Field', str], tuple[bool, str | None]]


class Field:
    def __init__(self, title: str, default=None, is_static=False, options: list[str]=None, 
                 allow_options_only=False, validation: Validation=None, prompt: str=None, css="", **kwargs):
        """Field constructor

        @param title: title that is used to access a field by name
        @param default: default value of the field.entry if not modified by the test operator
        @param is_static: if True the field.entry is determined programmatically and not asked of the test operator
        @param options: a list of possible entry options that may be presented to the test operator
        @param allow_options_only: if True the test operator can only select from the list of options and is not able to provide a different entry value
        @param validation: function that performs entry validation to scrub input from the test operator
        @param prompt: text prompt to display when asking the test operator for the field
        @param css: a set of CSS classes to modify the field's GUI environment when test operator is prompted
        """
        self.title: str = title

        # Search the supplied command line arguments for a supplied entry value
        # Example Arg: -FMyField="My Entry Value"
        #     field.title = "MyField"
        #     field.entry = "My Entry Value"
        argval = argsF.get(self.title, None)
        if argval is not None:
            self.is_argument: bool = True
            self.default: str = argval
            self.is_static: bool = True
        else:
            self.is_argument: bool = False
            self.default: str = default
            self.is_static: bool = is_static

        self.prompt: str = prompt or title
        self.validation: Validation = validation or Field._default_validation
        self.options: list[str] = options
        self.allow_options_only = allow_options_only
        self.css_classes: str = css

        self.entry: str = self.default

    @staticmethod
    def _default_validation(field: str, entry: str) -> tuple[bool, str | None]:
        """
        @param field: 
        """
        if not entry or len(entry) == 0:
            return False, "{} must not be empty.".format(field.title)
        else:
            return True, ""

    def evaluate(self):
        return text_eval(self.entry)

class FieldCollection:
    def __init__(self):
        self.fields = {}

    def get_entry(self, title: str, default=None) -> str | None:
        """Gets a field by name and returns its entry value

        @param title: Field title
        @param default: default value if Field doesn't exist
        @return: Field.entry of specified titled Field
        """
        f = self.fields.get(title)
        if f:
            return f.entry
        return default

    def get_entries(self, *titles: str) -> tuple[str, ...]:
        """Gets many fields by name and returns their entry values as a tuple
            if field doesn't exist a None will fill its corresponding tuple index

        @param titles: set of Field titles
        @return: tuple of Field.entry values, one per each title requested, always
        """
        return tuple(self.get_entry(title) for title in titles)

    def update_entry(self, title: str, entry: str) -> bool:
        """Updates the entry value of a field by title

        @param title: title of field
        @param entry: new entry value for field
        @return: True if update succeeded, False if no field was found
        """
        f = self.fields.get(title)
        if f:
            f.entry = entry
            return True
        return False

    def update_entries(self, entrydict: dict[str, str]):
        """Updates many field entries

        @param entrydict: dictionary where keys are field titles and values are desired field entries
        @return: True if all updates succeeded, False if one or more update failed
        """
        all_succeeded = True
        for title, entry in entrydict.items():
            all_succeeded &= self.update_entry(title, entry)
        return all_succeeded

    def add(self, *fields):
        for field in fields:
            self.fields[field.title] = field

    def add_new(self, title, default=None, is_static=False, options=None, allow_options_only=False,
                validation=None, prompt=None, css=tuple(), **kwargs):
        """Create new field and add it to this field collection

        @param title: title that is used to access a field by name
        @param default: default value of the field.entry if not modified by the test operator
        @param is_static: if True the field.entry is determined programmatically and not asked of the test operator
        @param options: a list of possible entry options that may be presented to the test operator
        @param allow_options_only: if True the test operator can only select from the list of options and is not able to provide a different entry value
        @param validation: function that performs entry validation to scrub input from the test operator
        @param prompt: text prompt to display when asking the test operator for the field
        @param css: a set of CSS classes to modify the field's GUI environment when test operator is prompted
        """
        self.add(Field(title, default, is_static, options, allow_options_only, validation, prompt, css, **kwargs))

    def _get_structure(self) -> dict[str, Any]:
        flist = []
        for field in self.fields.values():
            fobj = {
                "title": field.title,
                "is_static": field.is_static,
                "default": field.default,
                "allow_options_only": field.allow_options_only,
                "entry": field.entry,
                "is_argument": field.is_argument,
                "options": field.options,
            }
            flist.append(fobj)
        return flist

    # Dictionary Passthrough Methods
    def clear(self):
        self.fields.clear()

    def get(self, key: str, default=None) -> Field:
        return self.fields.get(key, default)

    def __getitem__(self, key: str) -> Field:
        return self.fields[key]

    def __setitem__(self, key: str, val: Field):
        self.fields[key] = val

    def __delitem__(self, key):
        del self.fields[key]

    def __iter__(self) -> Iterator[Field]:
        return self.fields.__iter__()

    def items(self) -> Iterator[tuple[str, Field]]:
        return self.fields.items()

    def update(self, adict=None):
        self.fields.update(adict)
