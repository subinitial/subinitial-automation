import subprocess
import json
import sys
import time
import datetime
import os
import traceback

class times:
    start = datetime.datetime.now()
    end = datetime.datetime.now()


def _run(logdir):
    times.start = datetime.datetime.now().strftime("%y-%m-%d_%H-%M-%S")

    args = [sys.executable] + list(sys.argv) + ["--si-autorun"]
    print("executing:", args)
    my_env = os.environ.copy()
    my_env["PYTHONUNBUFFERED"] = "1"
    my_env["PYTHONIOENCODING"] = "utf8"
    my_env["SI_REPEAT"] = "1"
    proc = subprocess.Popen(args, stdout=subprocess.PIPE, env=my_env)
    lastline = "-"
    lines = []
    while True:
        line = proc.stdout.readline().decode()
        if line == '':
            break
        lastline = line
        print(line, end="")
        lines.append(line)

    times.end = datetime.datetime.now().strftime("%y-%m-%d_%H-%M-%S")
    with open(logdir + "/stdout_{}.txt".format(times.end), mode="w", encoding="utf-8") as f:
        for line in lines:
            f.write(line)

    return json.loads(lastline)


def repeat(logdir="reports/logs", repeat_delay=3.0):
    os.makedirs(logdir, exist_ok=True)
    with open(logdir + '/runstats.csv', mode='a') as f:
        f.write("{},{},{},{},{},{},{},{},{}\n".format("Run", "Start", "Finish", "Pass", "Fail", "Except", "PassCount",
                                                      "FailCount", "Exceptions"))
    i = 1
    passcount_total = 0
    failcount_total = 0
    exception_total = 0
    try:
        while True:
            passcount = 0
            failcount = 0
            excepted = 0
            try:
                passcount, failcount = _run(logdir)
                passcount_total += passcount
                failcount_total += failcount
            except KeyboardInterrupt as ex:
                break
            except Exception as ex:
                exception_total += 1
                excepted = 1
                print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
                      "\nException: '{}'\n{}>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".format(ex, traceback.print_exc()))

            with open(logdir + '/runstats.csv', mode='a') as f:
                f.write("{},{},{},{},{},{},{},{},{}\n".format(i, times.start, times.end,
                                                           passcount, failcount, excepted,
                                                           passcount_total, failcount_total, exception_total))


            print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"
                  "Run# {}, Passed: {}, Failed: {}, Errored: {}\n"
                  ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".format(
                i, passcount_total, failcount_total, exception_total))

            print("restarting in {} seconds...".format(repeat_delay))
            time.sleep(repeat_delay)
            i += 1
    except KeyboardInterrupt:
        pass

    print("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"
          "Total: {}, Passed: {}, Failed: {}, Errored: {}\n"
          ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".format(
            i, passcount_total, failcount_total, exception_total))
