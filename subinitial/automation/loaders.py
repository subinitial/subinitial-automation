# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework config setting loaders
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import json


class JsonLoader:
    def __call__(self, testnode, filename):
        self.load(testnode, filename)

    def __init__(self):
        self.testidx = 1
        self.csv = ""

    def load(self, testnode, filename):
        config = None
        with open(filename, 'r') as jsonfile:
            config = json.load(jsonfile)
        self._readjsonnodes(testnode, config)

    def _readjsonnode(self, testnode, config):
        type = config.get("type", None)

        if type is None or type == testnode._steptype:
            config.pop("type", None)
            child_configs = None
            if len(testnode.steps):
                child_configs = config.pop("children", None)

            #consume the remaining properties
            for key in config:
                setattr(testnode, key, config[key])
            return True, child_configs
        else:
            return False, None

    def _readjsonnodes(self, testnode, config):
        success, child_configs = self._readjsonnode(testnode, config)
        if not success:
            return False

        if child_configs is not None and len(testnode.steps) > 0:
            chiter = iter(testnode.steps)
            for child_config in child_configs:
                try:
                    child = next(chiter)
                    while self._readjsonnodes(child, child_config) is not True:
                        child = next(chiter)
                except StopIteration:
                    break

        return True
