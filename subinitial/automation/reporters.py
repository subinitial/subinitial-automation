# -*- coding: UTF-8 -*-
# Subinitial Test Automation Framework test-step result reporters
# © 2012-2022 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import os
import sys
import json
import threading
from typing import TYPE_CHECKING, Optional, Union
import typing

from subinitial.automation.assertions import AssertionBase
from .utils import NodeString

if TYPE_CHECKING:
    from .field import Field
    from .step import Step
    from .testdefinition import Action, TestDefinition
    

from .lib.colorama import init, Fore, Style
if "SI_ANSI" in os.environ:
    strip = os.environ["SI_ANSI"] == '0'
else:
    strip = False if os.environ.get("PYTHONUNBUFFERED", False) or "win" not in sys.platform.lower() else True
init(autoreset=True, strip=strip)



class Reporter:
    def __init__(self, test):
        self.test: "TestDefinition" = test
        self.buffers = {}

    def report_guictrl(self, arg):
        pass

    def report_start(self, step: "Step", blocking=True):
        pass

    def report_connecting(self):
        pass

    def report_connected(self):
        pass

    def report_disconnecting(self):
        pass

    def report_disconnected(self):
        pass

    def report_metadata(self, data):
        pass

    def report_action(self, action: "Action"):
        pass

    def report_run(self, step: "Step"):
        pass

    def report_stepstart(self, step: "Step"):
        pass

    def report_assertions(self, step: "Step"):
        pass

    def report_criteria(self, step: "Step"):
        pass

    def report_stepresult(self, step: "Step"):
        pass

    def report_stepfinishedasync(self, step: "Step"):
        pass

    def report_summary(self, test: "TestDefinition"):
        pass

    def report_info(self, infotext: str, step: Optional["Step"]=None, clear=False):
        pass

    def report_error(self, error: Exception | str, traceback="", step: Optional["Step"]=None):
        pass

    def prompt_continue(self, promp, can_cancel=False, blocking=True, step: Optional["Step"]=None, response=None, title: str | None=None, markdown=False):
        return None

    def prompt_input(self, prompt, blocking=True, step: Optional["Step"]=None, response=None, title: str | None=None, markdown=False):
        return None

    def prompt_field(self, field, blocking=True, step: Optional["Step"]=None):
        return None

    def prompt_fields(self, *fields, blocking=True, step: Optional["Step"]=None):
        return tuple(self.prompt_field(f, blocking) for f in fields)


class ConsoleReporter(Reporter):
    _fail = Style.BRIGHT+Fore.RED
    _warn = Style.BRIGHT+Fore.YELLOW
    _pass = Fore.GREEN

    _header = Style.BRIGHT+Fore.YELLOW
    _title_start = Fore.CYAN
    _title_end = "" #Fore.CYAN
    _title_flags = Fore.YELLOW

    _clearfix = Style.RESET_ALL

    def __init__(self, test):
        Reporter.__init__(self, test)
        self._lock_report = threading.Lock()

    def report(self, line, step: Optional["Step"]=None, end="\n", flush=False, clear=False):
        if step is not None and step._thread_step:
            with self._lock_report:
                thread_step = step._thread_step
                buffer = self.buffers.get(thread_step)
                if thread_step._was_active(wait=False):
                    if buffer:
                        for preline in buffer:
                            sys.stdout.write(preline)
                            # print(preline)
                        self.buffers.pop(thread_step)
                    print(line, end=end)
                    return
                if not buffer:
                    buffer = self.buffers[thread_step] = []
            buffer.append(line)
            buffer.append(end)
        else:
            if clear:
                sys.stdout.write("\033[F\033[K")
            print(line, end=end, flush=flush)

    _restostr = {True: _pass+"PASS"+_clearfix, False: _fail+"FAIL"+_clearfix, None: _warn+"SKIP"+_clearfix, "N/A": "N/A ", "FINAL": "FINL"}
    _restotext = {True: "PASS", False: _fail+"FAIL"+_clearfix, None: "SKIP", "N/A": "N/A "}

    def prompt_field(self, field:  Union["Field", str], blocking=True, step: Optional["Step"]=None):
        """Prompt the user for field value

        :param field:
        :type field: subinitial.automation.step.Field or str
        :returns: field entry value
        :rtype: str
        """
        if not blocking:
            return None

        if type(field) == str:
            while True:
                entry = self.prompt_input(field) or ""
                return entry

        if field.is_static or field.is_argument:
            self.report("{}: {}".format(field.title, field.entry), step)
            return field.entry  # entry is assigned default on initialization

        # determine the prompt hint
        hint = None
        if field.options:
            hint = "{{{}}}[{}]".format(
                ", ".join('{}: {}'.format(idx, option) for idx, option in enumerate(field.options)),
                field.default)
        elif field.default:
            hint = "[{}]".format(field.default)

        # determine the prompt
        prompt = "{}{}:".format(field.prompt, hint) if hint else "{}:".format(field.prompt)

        # create a getter
        getter = lambda: self.prompt_input(prompt, blocking, step) or field.default or ""

        while True:
            entry = getter().strip()

            if field.options:  # Try to convert option selection to entry value
                try: entry = field.options[int(entry)]
                except (IndexError, ValueError):
                    if field.allow_options_only and entry not in field.options:
                        self.report("Invalid option: {}".format(entry), step)
                        continue

            valid, message = field.validation(field, entry)
            if valid:
                return entry
            elif message:
                self.report(message, step)
            

    def report_connecting(self):
        self.report("Connecting...")

    def report_connected(self):
        self.report("Connected")

    def report_disconnecting(self):
        self.report("Disconnecting...")

    def report_disconnected(self):
        self.report("Disconnected")

    def report_action(self, action: "Action"):
        pass
        # self.report("Action: {} -> {}".format(action.title, action.retval))

    def report_start(self, test: "TestDefinition", blocking=True):
        self.report("\n" + self._header + test.title + self._clearfix)
        if test.skip_field_prompt:
            return

        if blocking:
            self.report("press [Enter] to start, 'h' for help, 'q' to quit...")
            while True:
                input = self.prompt_input("", blocking=True).strip()
                if input == "":
                    if test._should_reload_config():
                        input = self.prompt_input(
                            "the test config file has changed, press [Enter] to continue, 'y' to reload: ")
                        if input.lower() == 'y':
                            return "reload"
                    break
                intxt = input.lower().split()
                if intxt[0] == "q":
                    return "close"
                if intxt[0] == "h":
                    self.report("\n" + self._header + test.title + self._clearfix)
                    self.report("press [Enter] to continue, 'h' for help, 'q' to quit...")
                    self.report("'r #' - repeat test in debug mode every # seconds (defaults to 3s)")
                elif intxt[0] == "r":
                    repeat_delay = 3
                    try: repeat_delay = float(intxt[1])
                    except: pass
                    
                    from subinitial.automation.repeat import repeat
                    try:
                        repeat(repeat_delay=repeat_delay)
                    except Exception as ex:
                        print(ex)
                    exit(0)
                else:
                    self.report("\tinvalid entry: {}".format(input))

        fields = []
        if blocking:
            for key, field in test.fields.items():
                if not field.is_static:
                    fields.append(field)

        # self.report("\n" + self._header + test.title)
        # if not len(fields):
        #     self.report(self._clearfix)
        if len(fields):
            self.report("\n{}Field Entry ({}){}".format(self._header, len(fields), self._clearfix))

        for field in fields:
            field.entry = self.prompt_field(field)

    def report_run(self, test: "TestDefinition", blocking=True):
        self.report("\n" + self._header + test.report.title + self._clearfix)

        for key, field in test.fields.items():
            self.report("{}: {}".format(field.title, field.entry))

        self.report(self._header+"\n TID:A#  RSLT  TEST-PROCEDURE-TREE"+self._clearfix)

    def report_stepstart(self, step: "Step"):
        idxstr = "{:0>3}".format(step._index)
        nodestr = NodeString.node(step._get_depth()-1, step._decorate_title(self._title_start, self._title_flags))
        self.report("{:>4}           {}{}".format(idxstr, nodestr, self._clearfix), step)

    def report_assertions(self, step: "Step", *assertions: AssertionBase):
        idxstr = "{:0>3}".format(step._index)
        step_depth = step._get_depth()-1
        passna = step.step_resultstyle == "pass-n/a"
        for assertion in assertions:
            if passna:
                result = "    "
                if assertion.result:
                    assertion.passtext = "SATISFIED"
                else:
                    assertion.failtext = "UNSATISFIED"
            else:
                if assertion.result == "":
                    result = "    "
                elif assertion.result:
                    result = self._pass+"PASS"
                elif assertion.result is None:
                    result = self._warn+"SKIP"
                else:
                    result = self._fail+"FAIL"
                    assertion.failtext = self._fail+"FAIL"
            nodestr = NodeString.child(step_depth, str(assertion))
            self.report("{:>4}:{:0>2}  {}{}  {}{}".format(idxstr, assertion._index, result, self._clearfix, nodestr, self._clearfix), step)

    def report_stepresult(self, step: "Step"):
        result = self._restostr.get(step.result, step.result)
        resulttext = self._restotext.get(step.result, step.result)
        idxstr = "{:0>3}".format(step._index)
        nodestr = NodeString.lastchild(step._get_depth()-1, "{}: {} (Duration: {:0.2f}s)".format(step.title, resulttext, step.run_time))
        self.report("{:>4}     {}{}  {}".format(idxstr, result, self._clearfix, nodestr), step)

    def report_stepfinishedasync(self, step: "Step"):
        buffer = self.buffers.get(step)
        if buffer:
            self.buffers.pop(step)
            for line in buffer:
                self.report(line, end="")

    def report_summary(self, test: "TestDefinition"):
        pass_not_fail = False
        if test._abort:
            result = self._fail + "ABORTED"
        elif test.result:
            pass_not_fail = True
            result = self._pass + "PASSED"
        else:
            result = self._fail + "FAILED"
        self.report("\n{}:\n{}{} (Duration: {}s)\n".format(test.title, result, self._clearfix, round(test.run_time, 2)))

        passedcolor = self._pass if test.report.passcount > 0 else self._warn
        self.report(passedcolor + "STEPS PASSED : " + str(test.report.passcount) + self._clearfix)

        if test.report.failcount == 0:
            self.report(passedcolor + "STEPS FAILED : 0" + self._clearfix)
        else:
            self.report(self._fail + "STEPS FAILED : " + str(test.report.failcount) + self._clearfix)

        if test.report.assertion_evalcount == 0:
            self.report("\tNO ASSERTIONS EVALUATED")
        elif test.report.assertion_failcount == 0:
            self.report("\tALL ASSERTIONS PASSED")
        else:
            self.report("\tASSERTIONS FAILED: {}{}{}".format(self._fail, str(test.report.assertion_failcount), self._clearfix))
        return pass_not_fail

    def prompt_continue(self, prompt, can_cancel=False, blocking=True, step: Optional["Step"]=None, response=None, title: str | None=None, markdown=False):
        if not blocking:
            prefix = self._prefix(step)
            lines = prompt.splitlines()
            for line in lines:
                self.report(prefix + self._warn + line + self._clearfix)
            if response:
                self.report(prefix + "[continued]")
            else:
                self.report(prefix + "[cancelled]")
            return None
        if title:
            prefix = self._prefix(step)
            self.report("\n" + prefix + self._warn + title + self._clearfix)
        if can_cancel:
            while True:
                resp = self.prompt_input(
                    "\n{} ... press [Enter] to continue, 'c' to cancel:".format(prompt),
                    blocking=blocking, step=step).strip().lower()
                if resp == "c":
                    return False
                elif resp == "":
                    return True
        else:
            self.prompt_input("\n{} ... press [Enter] to continue".format(prompt),
                              blocking=blocking, step=step)
            return True

    def prompt_input(self, prompt, blocking=True, step: Optional["Step"]=None, response=None, title: str | None=None, markdown=False):
        prefix = self._prefix(step)
        lines = prompt.splitlines()

        for line in lines:
            self.report(self._clearfix + prefix + self._warn + line + self._clearfix, step)

        if not blocking:
            self.report(prefix + "[" + str(response) + "]", step)
            return None
        else:
            # wait for step activation before we block on input:
            if step and step._thread_step:
                step._thread_step._was_active(wait=True)
            self.report(prefix + ">", step, end="")  # this report will flush any buffered lines if step was async threaded
            return input()

    def _prefix(self, step: Optional["Step"]=None):
        if step and step._depth:
            return " " * 15 + NodeString.notes_pre(step._depth-1)
        if self.test._depth:
            return " " * 15 + NodeString.notes_pre(self.test._depth-1)
        return ""

    def report_info(self, infotext: str, step: Optional["Step"]=None, clear=False):
        prefix = self._prefix(step) + self._warn
        lines = infotext.splitlines()
        for line in lines[0:-1]:
            self.report(prefix + line + self._clearfix, step, clear=clear)
        if lines:
            self.report(prefix + lines[-1] + self._clearfix, step, clear=clear)

    def report_error(self, error: Exception | str, traceback="", step: Optional["Step"]=None):
        infotext = "{}Error:{} {}".format(self._fail, self._clearfix, error)
        prefix = self._prefix(step)

        self.report(prefix + infotext, step)
        if traceback:
            prefix += "    "
            for line in traceback.split("\n"):
                self.report(prefix + line, step)


class StringEncoder(json.JSONEncoder):
    def default(self, obj):
        return str(obj)


class InteractiveReporter(Reporter):
    def __init__(self, test: "TestDefinition", commander):
        self.commander = commander
        """:type: subinitial.automation.interactive.Commander"""
        Reporter.__init__(self, test)

    def notify(self, obj):
        write, flush = self.commander._varnotify
        if not write:
            return
        write(json.dumps(obj, cls=StringEncoder))
        write('\n')
        flush()

    def report_connecting(self):
        self.notify({"cmd": "connecting"})

    def report_connected(self):
        self.notify({"cmd": "connected"})

    def report_disconnecting(self):
        self.notify({"cmd": "disconnecting"})

    def report_disconnected(self):
        self.notify({"cmd": "disconnected"})

    def report_action(self, action: "Action"):
        self.notify({"cmd": "actions", "actions": [action._get_structure()]})
    
    def report_guictrl(self, arg):
        self.notify({"cmd": "guictrl", "arg": arg})

    def report_start(self, atp: "TestDefinition", blocking=True) -> str | None:
        self.notify({"cmd": "start", "fields": atp.fields._get_structure()})
        if not self.test.skip_field_prompt:
            while True:
                cmd, args = self.test._wait_start(self.commander)
                if cmd in ("close", "reload"):
                    return cmd

                shouldreload = atp._should_reload_config()
                if shouldreload:
                    if self.commander.ask(
                            {"cmd": "should_reload", "oldmtime": atp.config.mtime, "newmtime": shouldreload}) \
                            .get("reload", False):
                        return "reload"

                fields, station, params = args  # params = {mtime, target, final}
                atp.station = station
                error_messages = []
                if fields:
                    for field in fields:
                        fieldobj = atp.fields[field["title"]]
                        entry = field["entry"]
                        valid, message = fieldobj.validation(fieldobj, entry)
                        if valid:
                            fieldobj.entry = field["entry"]
                        else:
                            error_messages.append(message)

                if error_messages:
                    self.report_disconnected()
                    self.prompt_continue("Invalid Fields:\n\t- " + "\n\t- ".join(error_messages))
                else:
                    break

    def report_metadata(self, data):
        self.notify({
            "cmd": "metadata",
            "data": data
        })

    def report_run(self, atp: "TestDefinition"):
        self.notify({
            "cmd": "run",
            "fields": atp.fields._get_structure()
        })

    def report_stepstart(self, step: "Step"):
        self.notify({"cmd": "stepstart", "tid": step.index})

        if step.toplevel._check_ctrl(self, self.commander, step.index):
            return True

    def report_assertions(self, step: "Step", *assertions: AssertionBase):
        if step.stage == step.Stage.CRITERIA:
            return
        elif step.stage == step.Stage.PROCEDURE:
            cmd = "assertions"
        else:
            cmd = "criteria"

        results = []
        resp = {"cmd": cmd, "tid": step.index, "assertions": results}
        for ass in assertions:
            results.append(ass.to_dict(False))
        self.notify(resp)

    def report_criteria(self, step: "Step"):
        self.report_assertions(step, *step.assertions)  # [step._assidx_criteria:])

    def report_stepresult(self, step: "Step"):
        result = step.result
        resp = {"cmd": "stepfinish", "tid": step.index, "result": result}

        if result is True:
            resp["_result"], resp["resultclass"] = ("PASS", "pass")
        elif result is False:
            resp["_result"], resp["resultclass"] = ("FAIL", "fail")
        elif result is None:
            resp["_result"], resp["resultclass"] = ("SKIP", "skip")
        elif result == "N/A":
            resp["_result"], resp["resultclass"] = ("N/A", "na")
        elif result == "EXIT":
            resp["_result"], resp["resultclass"] = ("EXIT", "")
        else:
            result = str(result)
            resp["_result"], resp["resultclass"] = (result, "")

        resp["duration"] = step.run_time
        resp["procedure_time"] = step.procedure_time
        self.notify(resp)

    def report_summary(self, test: "TestDefinition"):
        self.notify({"cmd": "summary", "tid": test.index,
                     "result": test.result,
                     "aborted": test._abort,
                     "skipcount": test.report.skipcount,
                     "passcount": test.report.passcount,
                     "failcount": test.report.failcount,
                     "assertion_failcount": test.report.assertion_failcount,
                     "duration": test.run_time})

    def report_stepfinishedasync(self, step: "Step"):
        obj = {"cmd": "async_finished"}
        if step:
            obj["tid"] = step.index
        self.notify(obj)

    def report_info(self, infotext: str, step: Optional["Step"]=None, clear=False):
        obj = {"cmd": "info", "text": infotext, "clear": clear}
        if step:
            obj["tid"] = step.index
        self.notify(obj)

    def report_error(self, error: Exception | str, traceback="", step: Optional["Step"]=None):
        obj = {"cmd": "error", "error": str(error), "traceback": traceback}
        if step:
            obj["tid"] = step.index
        self.notify(obj)

    def prompt_input(self, prompt, blocking=True, step: Optional["Step"]=None, title: str | None=None, markdown=False):
        if not blocking:
            return None
        req = {"cmd": "prompt_input", "prompt": prompt, "title": title, "markdown": markdown}
        if step:
            req["tid"] = step.index
            if step._thread_step:
                step._thread_step._was_active(wait=True)
        resp = self.commander.ask(req)
        return resp.get("input", None)

    def prompt_continue(self, prompt, can_cancel=False, blocking=True, step: Optional["Step"]=None, title: str | None=None, markdown=False):
        if not blocking:
            return None
        req = {"cmd": "prompt_continue", "can_cancel": can_cancel, "prompt": prompt, "title": title, "markdown": markdown}
        if step:
            req["tid"] = step.index
            if step._thread_step:
                step._thread_step._was_active(wait=True)
        resp = self.commander.ask(req)
        return resp.get("result", False)

    def prompt_field(self, field, blocking=True, step: Optional["Step"]=None):
        return self.prompt_fields(field, blocking=blocking, step=step)

    def prompt_fields(self, *fields, blocking=True, step: Optional["Step"]=None):
        if not blocking:
            return None
        f = []
        req = {"cmd": "prompt_fields", "fields": f}
        if step:
            req["tid"] = step.index
            if step._thread_step:
                step._thread_step._was_active(wait=True)
        for field in fields:
            f.append({"title": field.title})
        resp = self.commander.ask(req)


class LogReporter(ConsoleReporter):
    _fail = ""
    _warn = ""
    _pass = ""

    _header = ""
    _title_start = ""
    _title_end = ""
    _title_flags = ""

    _clearfix = ""

    _restostr = {True: "PASS", False: "FAIL", None: "SKIP"}
    _restotext = {True: "PASS", False: "FAIL", None: "SKIP"}

    def __init__(self, test: "TestDefinition", path):
        self.path = path
        self.f = None
        ConsoleReporter.__init__(self, test)

    def open(self):
        self.close()
        self.f = open(self.path, mode="a", encoding="utf-8")

    def close(self):
        if self.f:
            self.f.close()
            self.f = None

    def report_start(self, test: "TestDefinition", blocking=True):
        self.open()
        self.report("\n"+self._header+test.report.title, test)

        for key in test.fields:
            field = test.fields[key]
            self.report("{}: {}".format(field.title, field.entry), test)

        self.report(self._header+"\n TID.A#  RSLT  TEST-PROCEDURE-TREE", test)

    def report(self, text, step: Optional["Step"]=None):
        if not self.f:
            self.open()
        self.f.write(text)
        self.f.write('\n') # os.linesep)

    def prompt_input(self, prompt: str, blocking=True, step: Optional["Step"]=None):
        return None


class AggregateReporter(Reporter):
    def __init__(self, test: "TestDefinition", *reporters: Reporter):
        Reporter.__init__(self, test)
        if len(reporters) < 1:
            raise Exception("AggregateReporter must consist of at least one Reporter instance")
        self.reporters = reporters

    def report_start(self, step: "Step", blocking=True):
        retval = self.reporters[0].report_start(step, blocking)
        for r in self.reporters[1:]:
            r.report_start(step, False)
        return retval

    def report_disconnecting(self):
        for r in self.reporters:
            r.report_disconnecting()

    def report_connecting(self):
        for r in self.reporters:
            r.report_connecting()

    def report_connected(self):
        for r in self.reporters:
            r.report_connected()

    def report_action(self, action: "Action"):
        for r in self.reporters:
            r.report_action(action)

    def report_disconnected(self):
        for r in self.reporters:
            r.report_disconnected()

    def report_metadata(self, data):
        for r in self.reporters:
            r.report_metadata(data)

    def report_run(self, step: "Step"):
        for r in self.reporters:
            r.report_run(step)

    def report_stepstart(self, step: "Step"):
        retval = self.reporters[0].report_stepstart(step)
        for r in self.reporters[1:]:
            r.report_stepstart(step)
        return retval

    def report_assertions(self, step: "Step", *assertions: AssertionBase):
        for r in self.reporters:
            r.report_assertions(step, *assertions)

    def report_criteria(self, step: "Step"):
        for r in self.reporters:
            r.report_criteria(step)

    def report_stepresult(self, step: "Step"):
        for r in self.reporters:
            r.report_stepresult(step)

    def report_summary(self, test: "TestDefinition"):
        for r in self.reporters:
            r.report_summary(test)

    def report_stepfinishedasync(self, step: "Step"):
        for r in self.reporters:
            r.report_stepfinishedasync(step)

    def report_info(self, infotext: str, step: Optional["Step"]=None, clear=False):
        for r in self.reporters:
            r.report_info(infotext, step=step, clear=clear)

    def report_error(self, error: Exception | str, traceback="", step: Optional["Step"]=None):
        for r in self.reporters:
            r.report_error(error, traceback=traceback, step=step)

    def prompt_continue(self, prompt, can_cancel=False, blocking=True, step: Optional["Step"]=None, response=None, title: str | None=None, markdown=False):
        response = self.reporters[0].prompt_continue(prompt, can_cancel, blocking, step, title, markdown)
        for r in self.reporters[1:]:
            r.prompt_continue(prompt, can_cancel, False, step, response, title, markdown)
        return response

    def prompt_input(self, prompt: str, blocking=True, step: Optional["Step"]=None, response=None, title: str | None=None, markdown=False):
        response = self.reporters[0].prompt_input(prompt, blocking, step, title, markdown)
        for r in self.reporters[1:]:
            r.prompt_input(prompt, False, step, response, title, markdown)
        return response

    def prompt_field(self, field: "Field", blocking=True, step: Optional["Step"]=None):
        return self.reporters[0].prompt_field(field, blocking, step)

    def prompt_fields(self, *fields: "Field", blocking=True, step: Optional["Step"]=None):
        return self.reporters[0].prompt_fields(*fields, blocking, step)
