# Automation Project Template

## Requirements
- Python 3.10+

## Installing

CD to the ATP project root directory (where atp.py lives) then...


### GNU/Linux
```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -R requirements.txt
```

### Windows
```cmd.exe
python -m virtualenv .venv
.venv\Scripts\activate.bat
pip install -R requirements.txt
```


## Running
1) Activate the .venv virtual environment after installing
2) Execute `./atp.py` or `python atp.py` from the base directory 
   (actual command may vary per operating system and python version)

## Debugging
1) In Visual Studio Code, open the "Run and Debug" sidepanel, and in the dropdown select 
   "Python: ATP" and click the green run button. 
