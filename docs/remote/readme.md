# Remote Example

python3.10-12

## Windows

### On Server (test rack)

#### First Time Setup

```bash
cd ../template
python -m venv .venv
.venv\Scripts\Activate.ps1
pip install -e ../../. # install automation in the venv
```

#### Start the server

```bash
cd ../template
.venv\Scripts\Activate.ps1
python atp.py --si-interactive --si-address=:11235
```

### On Client (other user account, another PC, or another terminal window)

```bash
cd ../remote
python client.py --si-address=127.0.0.1:11235
```

## Linux

```bash
# in server terminal window
cd ../template
python3 -m venv .venv
.venv/bin/python3 atp.py --si-interactive --si-address=:11235

# in client (another) terminal window
.venv/bin/python3 ../remote/client.py --si-address=127.0.0.1:11235
```