#!/usr/bin/env python3
import socket
import io
import threading
import json
import queue
import logging
import typing
import argparse


logger = logging.getLogger(__name__)


def main():
    # logging.basicConfig(level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='Subinitial Automation Remote Client')
    parser.add_argument('-a', '--si-address', help='ipv4:tcp-port', default='', required=False)
    args = parser.parse_args()

    addr = args.si_address
    port = 11234
    if ":" in addr:
        addr, port = addr.split(":")
        port = int(port)
    
    rc = RemoteClient(handler=ExampleHandler())
    rc.connect(addr, port)

    print("remote-test connection established\ntest info", rc.get_test_info())

    input("ready?>")
    rc.start()

    input("")
    rc.close()


class ExampleHandler:
    """ 
    This object is provided to RemoteClient to give a set of methods to call when events are received
    from the test process.

    See InteractiveReporter on line 422 of subinitial/automation/reporters.py for the server-side.
    """
    
    def connecting(self):
        print("connecting")

    def connected(self):
        print("connected")

    def run(self, fields=[]):
        print("run", fields)

    def stepstart(self, tid=0):
        print("stepstart", tid)

    def info(self, tid=0, text="", clear=False):
        print("info", tid, text, clear)

    def criteria(self, tid=0, assertions=[]):
        print("criteria", tid, assertions)
    
    def stepfinish(self, tid=0, result=False, _result="", resultclass="", duration=0.0, procedure_time=0.0):
        print("stepfinish", tid, result, _result, resultclass, duration, procedure_time)

    def summary(self, tid=0, result=False, _result="", aborted=False, skipcount=0, passcount=0, failcount=0, 
                assertion_failcount=0, duration=0.0, resultclass=""):
        print("summary", tid, result, _result, resultclass, aborted, skipcount, passcount, failcount, assertion_failcount, duration)

    def disconnecting(self):
        print("disconnecting")

    def disconnected(self):
        print("disconnected")


class RemoteClient:
    def __init__(self, handler: typing.Any=None) -> None:
        self.is_running = False
        self.socket: socket.socket
        self.fin: io.TextIOWrapper
        self.fout: io.TextIOWrapper
        self.t_line: threading.Thread
        self.q_ask = queue.Queue()
        self._handlers: dict[str, typing.Any] = {}
        self.__mid = 1
        self.handler = handler

    def _next_mid(self) -> int:
        x = self.__mid
        if x >= 65535:
            self.__mid = 1
        else:
            self.__mid += 1
        return x

    def connect(self, host="127.0.0.1", port=11234):
        logger.info("connecting to: %s:%d", host, port)
        s = self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        self.fin = s.makefile(mode='r', encoding='utf-8', newline='\n')
        self.fout = s.makefile(mode='w', encoding='utf-8', newline='\n')
        self.is_running = True

        self.t_line = threading.Thread(target=self._task_line, daemon=True)
        self.t_line.start()

    def close(self):
        if self.is_running:
            self._send(cmd='q')
            self.is_running = False
            self.fout.close()
            self.socket.close()
            # self.fin.close()

    def get_test_info(self):
        """ @return {title: str, lib_version: tuple[int, int, int]} """
        return self._ask(cmd='t')

    def start(self, arg='', fields: list[dict] | None=None):
        """ start the test """
        op = {
            "cmd": "start",
            "arg": arg,
            "fields": fields or [],
            # "station": station,
        }
        return self._ask(cmd="ctrl", operation=op)

    def _ctrl(self, op: typing.Literal['resume', 'pause', 'stop', 'close']):
        return self._ask(cmd="ctrl", operation=op)

    def _ask(self, cmd='', **kwargs):
        '''
        Talk from client -> server.

        See the process_command method on line 48 of subinitial/automation/interactive.py for the server-side.
        '''
        self._handlers[cmd] = lambda resp: self.q_ask.put(resp)
        self._send(cmd=cmd, mid=self._next_mid(), **kwargs)
        r = self.q_ask.get()
        resp = r.get('response')
        if not resp:
            resp = r.get('result')
        return resp


    def _task_line(self):
        while self.is_running:
            line = self.fin.readline()
            logger.info("fin.readline() -> %s", line)
            msg = json.loads(line)
            cmd = msg.pop("cmd", None)
            handler = self._handlers.pop(cmd, None)
            if handler:
                handler(msg)
            else:
                meth = getattr(self.handler, cmd, None)
                if meth:
                    meth(**msg)

    def _send(self, **kwargs):
        txt = json.dumps(kwargs)
        logger.info("fout.write(%s)", txt)
        self.fout.write(txt + "\n")
        self.fout.flush()

        
if __name__ == "__main__":
    main()
