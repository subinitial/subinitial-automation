# Subinitial Automation Library

For more information visit this library's wiki at:
https://bitbucket.org/subinitial/subinitial-automation/wiki/Home

Subinitial Stacks information:
https://subinitial.com

Git repository:
https://bitbucket.org/subinitial/subinitial-automation.git


## Introduction

The Subinitial Automation Library a framework to design and run hardware automated tests.


## Requirements
- Python 3.10+

The Test Automation Framework is designed for Python 3.10+. Python 2 is not supported.
Depending on what you use from the subinitial.automation framework 3rd party dependencies are required.
All 3rd party dependencies have been included in subinitial.automation.lib and will be bootstrapped upon
import as fallbacks if your standard Python environment is missing any. The 3rd party libraries included have
been tested on Debian, Ubuntu, and Windows 10 (64 and 32bit).

Please refer to the Test Automation Framework page on more details regarding 3rd party library requirements.

### Python 3.10 on Ubuntu:
```bash
sudo bash -c "add-apt-repository ppa:deadsnakes/ppa -yu && \
  apt install -y python3.10 python3.10-venv python3.10-distutils"
```

## Installation

`python3.10 -m pip install --user git+https://bitbucket.org/subinitial/subinitial-automation.git`

NOTES:

- pip's `--user` flag allows you to install this package without admin privileges,
  If you'd like to install the package system-wide then omit the `--user` flag
- You can use virtualenv to make a localized Python environment for a particular project then
  pip install all required packages as needed with the virtualenv activated. Omit the `--user`
  flag when installing this package inside a virtualenv.

## Verify Installation
```
#!python3.10
from subinitial.automation import *
test_step = Step()
```

For more detailed installation instructions and help getting started refer to the [Getting Started](https://bitbucket.org/subinitial/subinitial/wiki/Getting_Started) page.


## Included Third-Party Libraries

Third-Party libraries have been in included for convenience in the package subdirectory:
    subinitial/automation/lib

### LICENSE AND COPYRIGHT

Each 3rd party python package has been included with subinitial.automation for automatic fall-back usage when these packages are not found in your existing python library. All are optional or situationally dependent. Included are web-links for each package, please refer to these websites for copyrights, licensing, and author information.

colorama - https://pypi.python.org/pypi/colorama
jdcal - https://pypi.python.org/pypi/jdcal
